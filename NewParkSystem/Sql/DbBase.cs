﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using SqlSugar;
using System.Data;
using System.IO;
using Sql.Model;

namespace Sql
{
    public class DbBase
    {
        public static string _connectionString = ConfigurationManager.AppSettings["DbConnect"].ToString();
        public static SqlSugarClient db;

        public static bool Open()
        {
            try
            {
                db = new SqlSugarClient(new ConnectionConfig()
                {
                    ConnectionString = _connectionString,
                    DbType = SqlSugar.DbType.SqlServer,
                    IsAutoCloseConnection = true,
                    InitKeyType = InitKeyType.Attribute
                });
                db.Open();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        /// <summary>
        /// 通过停车场ID获取功能列表
        /// </summary>
        /// <param name="parkingLot">停车场ID</param>
        /// <returns></returns>
        public static List<IPS_FunctionList> GetSetting(int parkingLot)
        {
            List<IPS_FunctionList> functionLists = new List<IPS_FunctionList>();
            try
            {
                functionLists = db.Queryable<IPS_FunctionList>().Where(it => it.ParkingLotId == parkingLot).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return functionLists;
        }

        public static bool SaveFunction(List<IPS_FunctionList> functionLists)
        {
            try
            {
                for (int i = 0; i < functionLists.Count; i++)
                {
                    var result = db.Updateable(functionLists[i]).Where(it => it.Id == functionLists[i].Id).ExecuteCommand();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
