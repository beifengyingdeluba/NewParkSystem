﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sql
{
    public class SqlDbBase
    {
        private static string _connectionString = Encryp.DecryptDES(ConfigurationManager.AppSettings["SqlConnect"].ToString(), Encryp.ToDES(Encryp.EncodeBase64(ConfigurationManager.AppSettings["EncKey"].ToString())));
        private static SqlSugarClient db;

        public static void Open()
        {
            try
            {
                db = new SqlSugarClient(new ConnectionConfig()
                {
                    ConnectionString = _connectionString,
                    DbType = SqlSugar.DbType.SqlServer,
                    IsAutoCloseConnection = true,
                    InitKeyType = InitKeyType.Attribute
                });
                db.Open();
            }
            catch (Exception)
            {
            }
        }
        public static void Close()
        {
            try
            {
                db.Close();
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// 通过传入paramters来调用存储过程
        /// </summary>
        /// <param name="procedureName">存储过程名</param>
        /// <param name="paraDir">要传入的参数的paramters</param>
        /// <returns></returns>
        public static DataTable UseParamtersProcedure(string procedureName, Dictionary<String, Object> paraDir)
        {
            DataTable dt = new DataTable();
            try
            {
                Open();
                dt = db.Ado.UseStoredProcedure().GetDataTable(procedureName, paraDir);

            }
            catch (Exception ex)
            {
            }
            finally
            {
                Close();
            }
            return dt;
        }

        /// <summary>
        /// 通过传入paramters来调用存储过程
        /// </summary>
        /// <param name="procedureName">存储过程名</param>
        /// <param name="paraDir">要传入的参数的paramters</param>
        /// <returns>返回dataset</returns>
        public static DataSet UseParamtersProcedureDataSet(string procedureName, Dictionary<String, Object> paraDir)
        {
            DataSet ds = new DataSet();
            try
            {
                Open();
                ds = db.Ado.UseStoredProcedure().GetDataSetAll(procedureName, paraDir);
            }
            catch (Exception)
            {
            }
            finally
            {
                Close();
            }
            return ds;
        }

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="dataType"></param>
        /// <param name="dtData"></param>
        /// <param name="procedureName"></param>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public static int SaveData(int dataType, DataTable dtData, string procedureName, ref string errorMsg)
        {
            int errorId = 1000;
            errorMsg = "";
            string xmlPara = String.Empty;
            try
            {
                Open();
                xmlPara = ConvertDataTableToXml(dtData);
                SugarParameter returnPara = new SugarParameter("@return", errorId);
                returnPara.Direction = ParameterDirection.ReturnValue;
                SugarParameter[] paras = {
                    new SugarParameter("@DataType",dataType),
                    new SugarParameter("@XMLData",xmlPara)
                };
                int count = db.Ado.UseStoredProcedure().ExecuteCommand(procedureName, paras);
                errorId = Convert.ToInt32(paras[2].Value);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Close();
            }
            return errorId;
        }

        #region 用XMLData保存数据
        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="dataType"></param>
        /// <param name="dtData"></param>
        /// <param name="procedureName"></param>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public static int SaveDataDT(int dataType, DataTable dtData, string procedureName, ref string errorMsg)
        {
            int errorId = 1000;
            errorMsg = "";
            string xmlPara = String.Empty;
            try
            {
                Open();
                xmlPara = ConvertDataTableToXml(dtData);
                SugarParameter returnPara = new SugarParameter("ErrorID", errorId);
                returnPara.Direction = ParameterDirection.ReturnValue;
                SugarParameter[] paras = {
                    new SugarParameter("DataType",dataType),
                    new SugarParameter("XMLData",xmlPara),
                    returnPara
                };
                int count = db.Ado.UseStoredProcedure().ExecuteCommand(procedureName, paras);
                errorId = Convert.ToInt32(paras[2].Value);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Close();
            }
            return errorId;
        }
        #endregion

        #region 生成方法
        #region  DataTable生成XML
        /// <summary>
        /// DataTable生成XML(集合)
        /// </summary>
        /// <typeparam name="dt">DataTable数据集</typeparam>
        /// <returns></returns>
        public static string ConvertDataTableToXml(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.AppendLine("<" + dt.TableName + ">");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        sb.AppendLine("<Rows>");
                        foreach (DataColumn col in dt.Columns)
                        {
                            sb.AppendLine("<" + col.ColumnName + ">");
                            sb.AppendLine("<![CDATA[" + row[col].ToString() + "]]>");
                            sb.AppendLine("</" + col.ColumnName + ">");
                        }
                        sb.AppendLine("</Rows>");
                    }
                }
                sb.AppendLine("</" + dt.TableName + ">");
            }
            catch (Exception ex)
            { }
            return sb.ToString();
        }
        #endregion
        #endregion
    }
}
