﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NewParkSystem.Models;
using Bealead.ICEIPC;
using Bealead.ICEIPC.Events;
using CCWin;
using CCWin.SkinControl;
using NewParkSystem.Tools.Class;
using NewParkSystem.Forms;
using Sql;
using NewParkSystem.Factory;
using NewParkSystem.Helper;
using System.Threading;

namespace NewParkSystem
{
    public partial class FrmMain : Skin_Mac
    {
        #region 定义全局变量
        List<CarPass> carPasses = new List<CarPass>();
        private Setting setting = new Setting();
        private ICEIPC mysdk = null;
        List<Guard> guards = new List<Guard>();
        List<Cond> entList = new List<Cond>();
        List<Cond> extList = new List<Cond>();
        List<SkinTabPage> skinTabPages = new List<SkinTabPage>();
        SkinTabPage tabPage = null;
        private static Helper.LogHelper log = LogFactory.GetLogger("PsFormLog");
        private Thread ThreadSetting = null;

        #region 窗口大小初始化变量
        AutoSizeFormClass asf = new AutoSizeFormClass();
        private Color normalBGColor = ColorTranslator.FromHtml("#1199FF");//几个常规的按钮背景颜色
        #endregion
        #endregion
        public FrmMain()
        {
            InitializeComponent();
            //this.ControlBox = false;   // 设置不出现关闭按钮
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            asf.controllInitializeSize(this);
            LoadConfig();
            this.WindowState = FormWindowState.Maximized;
            LoadCamera();
            //videoPic1_1.Load("https://jnirefuel.com/Image/IMG_1053.JPG");
            //CreateTabPage();
        }


        #region 载入配置文件
        private void LoadConfig()
        {
            Setting setting = new Setting();
            ConfigHelper<Setting>.LoadConfig(ref setting);
            Params.Settings = setting;
        }
        #endregion

        #region 显示
        #region 初始化窗口
        /// <summary>
        /// 初始化窗口
        /// </summary>
        private void InitScreen()
        {
            foreach (Guard guard in setting.Guards)
            {
                if (guard.Primary.ScreenType == 2)
                {

                }
                else
                {
                    if (mysdk != null)
                    {

                    }
                }
            }
        }
        #endregion

        #region 添加过车记录
        /// <summary>
        /// 添加过车记录
        /// </summary>
        /// <param name="plateNum"></param>
        /// <param name="trance"></param>
        public void AddPassLog(string plateNum, string trance)
        {
            if (carPasses.Count == 10)
            {
                carPasses.RemoveAt(0);
            }
            CarPass carPass = new CarPass();
            carPass.PlateNum = plateNum;
            carPass.DateTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            carPass.trance = trance;
            carPasses.Add(carPass);

            dataGradViewCarPass.Rows.Clear();
            dataGradViewCarPass.Rows.Add(carPasses);
            for (int i = 0; i < carPasses.Count; i++)
            {
                dataGradViewCarPass.Rows[i].Cells[0].Value = carPasses[i].PlateNum;
                dataGradViewCarPass.Rows[i].Cells[1].Value = carPasses[i].DateTime;
                dataGradViewCarPass.Rows[i].Cells[2].Value = carPasses[i].trance;
            }
        }
        #endregion

        #region 载入相机
        /// <summary>
        /// 载入相机
        /// </summary>
        private void LoadCamera()
        {
            Guard guard = new Guard();
            Cond cond = new Cond();
            int count = Params.Settings.Guards.Count;

            for (int i = 0; i < Params.Settings.Guards.Count; i++)
            {
                cond = new Cond();
                cond.IP = Params.Settings.Guards[i].Primary.IP;
                cond.VideoHwnd = GetHandle(i, cond.IP, Params.Settings.Guards[i].No + "_" + (Params.Settings.Guards[i].IsExit ? "入口" : "出口")).Handle;
                entList.Add(cond);
            }
            mysdk = new ICEIPC(entList.ToArray(), extList.ToArray());
            mysdk.IPCs.ForEach((l) =>
            {
                l.PlateEnterEvent += IPC_PlateEnterEvent;
                l.PlateExitEvent += IPC_PlateExitEvent;
                l.DeviceOnOffLineEvent += IPC_StatusChangeEvent;
                l.DeviceIOEvent += IPC_IOChangeEvent;
            });
            mysdk.PlateEvent = on_plate;
            mysdk.Init();
        }
        #endregion

        #region 绑定视频流句柄并生成对应Controls
        /// <summary>
        /// 绑定视频流句柄并生成对应Controls
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        private Control GetHandle(int i, string ip, string tranceName)
        {
            bool isFirstFour = true;
            Control control = new Control();

            int addPages = i / 4;
            int getValue = i % 4;
            if (addPages >= 1)
            {
                isFirstFour = false;
            }
            if (isFirstFour == true)
            {
                switch (getValue)
                {
                    case 0:
                        #region 初始化Tag
                        groupBoxVideo1.Tag = "";
                        groupBoxVideo2.Tag = "";
                        groupBoxVideo3.Tag = "";
                        groupBoxVideo4.Tag = "";

                        groupBoxVideoPic1_1.Tag = "";
                        groupBoxVideoPic1_2.Tag = "";
                        groupBoxVideoPic2_1.Tag = "";
                        groupBoxVideoPic2_2.Tag = "";
                        groupBoxVideoPic3_1.Tag = "";
                        groupBoxVideoPic3_2.Tag = "";
                        groupBoxVideoPic4_1.Tag = "";
                        groupBoxVideoPic4_2.Tag = "";

                        videoPic1_1.Tag = "";
                        videoPic1_2.Tag = "";
                        videoPic2_1.Tag = "";
                        videoPic2_2.Tag = "";
                        videoPic3_1.Tag = "";
                        videoPic3_2.Tag = "";
                        videoPic4_1.Tag = "";
                        videoPic4_2.Tag = "";
                        #endregion
                        groupBoxVideo1.Text = tranceName;
                        groupBoxVideo1.Tag = ip;
                        buttonUp1.Tag = ip;
                        videoPic1_1.Tag = "PictureEnter";
                        videoPic1_2.Tag = "PictureExit";
                        groupBoxVideoPic1_1.Tag = "Enter";
                        groupBoxVideoPic1_2.Tag = "Exit";
                        control = VideoBox1;
                        break;
                    case 1:
                        groupBoxVideo2.Text = tranceName;
                        groupBoxVideo2.Tag = ip;
                        buttonUp2.Tag = ip;
                        videoPic2_1.Tag = "PictureEnter";
                        videoPic2_2.Tag = "PictureExit";
                        groupBoxVideoPic2_1.Tag = "Enter";
                        groupBoxVideoPic2_2.Tag = "Exit";
                        control = VideoBox2;
                        break;
                    case 2:
                        groupBoxVideo3.Text = tranceName;
                        groupBoxVideo3.Tag = ip;
                        buttonUp3.Tag = ip;
                        videoPic3_1.Tag = "PictureEnter";
                        videoPic3_2.Tag = "PictureExit";
                        groupBoxVideoPic3_1.Tag = "Enter";
                        groupBoxVideoPic3_2.Tag = "Exit";
                        control = VideoBox3;
                        break;
                    case 3:
                        groupBoxVideo4.Text = tranceName;
                        groupBoxVideo4.Tag = ip;
                        buttonUp4.Tag = ip;
                        videoPic4_1.Tag = "PictureEnter";
                        videoPic4_2.Tag = "PictureExit";
                        groupBoxVideoPic4_1.Tag = "Enter";
                        groupBoxVideoPic4_2.Tag = "Exit";
                        control = VideoBox4;
                        break;
                }
            }
            else
            {
                if (skinTabPages.Count != addPages)
                {
                    tabPage = new SkinTabPage(i.ToString());
                    tabControl1.TabPages.Add(tabPage);
                    skinTabPages.Add(tabPage);
                }
                switch (getValue)
                {
                    case 0:
                        SkinPictureBox skinPicture1 = new SkinPictureBox();
                        SkinGroupBox skinGroupBox1 = new SkinGroupBox();
                        skinGroupBox1.Location = groupBoxVideo1.Location;
                        skinGroupBox1.Size = groupBoxVideo1.Size;
                        //skinGroupBox1.Text = getValue.ToString();
                        skinGroupBox1.BorderColor = groupBoxVideo1.BorderColor;
                        skinPicture1.Location = VideoBox1.Location;
                        skinPicture1.Size = VideoBox1.Size;
                        skinGroupBox1.Text = tranceName;
                        skinGroupBox1.Tag = groupBoxVideo1.Tag;

                        SkinGroupBox groupboxPicEnter = new SkinGroupBox();
                        groupboxPicEnter.Location = groupBoxVideoPic1_1.Location;
                        groupboxPicEnter.Size = groupBoxVideoPic1_1.Size;
                        groupboxPicEnter.Text = groupBoxVideoPic1_1.Text;
                        groupboxPicEnter.Tag = groupBoxVideoPic1_1.Tag;

                        SkinGroupBox groupboxPicExit = new SkinGroupBox();
                        groupboxPicExit.Location = groupBoxVideoPic1_2.Location;
                        groupboxPicExit.Size = groupBoxVideoPic1_2.Size;
                        groupboxPicExit.Text = groupBoxVideoPic1_2.Text;
                        groupboxPicExit.Tag = groupBoxVideoPic1_2.Tag;

                        SkinButton buttonUP = new SkinButton();
                        buttonUP.BaseColor = buttonUp1.BaseColor;
                        buttonUP.Text = buttonUp1.Text;
                        buttonUP.Size = buttonUp1.Size;
                        buttonUP.ForeColor = buttonUp1.ForeColor;
                        buttonUP.Location = buttonUp1.Location;
                        buttonUP.Font = buttonUp1.Font;
                        buttonUP.Tag = ip;

                        skinGroupBox1.Controls.Add(buttonUP);
                        skinGroupBox1.Controls.Add(skinPicture1);
                        skinGroupBox1.Controls.Add(groupboxPicEnter);
                        skinGroupBox1.Controls.Add(groupboxPicExit);
                        tabPage.Controls.Add(skinGroupBox1);
                        control = skinPicture1;
                        break;
                    case 1:
                        SkinPictureBox skinPicture2 = new SkinPictureBox();
                        SkinGroupBox skinGroupBox2 = new SkinGroupBox();
                        skinGroupBox2.Location = groupBoxVideo2.Location;
                        skinGroupBox2.Size = groupBoxVideo2.Size;
                        //skinGroupBox1.Text = getValue.ToString();
                        skinGroupBox2.BorderColor = groupBoxVideo2.BorderColor;
                        skinGroupBox2.Text = tranceName;
                        skinGroupBox2.Tag = groupBoxVideo2.Tag;


                        skinPicture2.Location = VideoBox2.Location;
                        skinPicture2.Size = VideoBox2.Size;

                        SkinGroupBox groupboxPicEnter2 = new SkinGroupBox();
                        groupboxPicEnter2.Location = groupBoxVideoPic2_1.Location;
                        groupboxPicEnter2.Size = groupBoxVideoPic2_1.Size;
                        groupboxPicEnter2.Text = groupBoxVideoPic2_1.Text;
                        groupboxPicEnter2.Tag = groupBoxVideoPic2_1.Tag;

                        SkinGroupBox groupboxPicExit2 = new SkinGroupBox();
                        groupboxPicExit2.Location = groupBoxVideoPic2_2.Location;
                        groupboxPicExit2.Size = groupBoxVideoPic2_2.Size;
                        groupboxPicExit2.Text = groupBoxVideoPic2_2.Text;
                        groupboxPicExit2.Tag = groupBoxVideoPic2_2.Tag;

                        SkinButton SkinbuttonUP2 = new SkinButton();
                        SkinbuttonUP2.BaseColor = buttonUp2.BaseColor;
                        SkinbuttonUP2.Text = buttonUp2.Text;
                        SkinbuttonUP2.Size = buttonUp2.Size;
                        SkinbuttonUP2.Location = buttonUp2.Location;
                        SkinbuttonUP2.ForeColor = buttonUp2.ForeColor;
                        SkinbuttonUP2.Font = buttonUp2.Font;

                        skinGroupBox2.Controls.Add(SkinbuttonUP2);
                        skinGroupBox2.Controls.Add(skinPicture2);
                        skinGroupBox2.Controls.Add(groupboxPicEnter2);
                        skinGroupBox2.Controls.Add(groupboxPicExit2);
                        tabPage.Controls.Add(skinGroupBox2);
                        control = skinPicture2;
                        break;
                    case 2:
                        SkinPictureBox skinPicture3 = new SkinPictureBox();
                        SkinGroupBox skinGroupBox3 = new SkinGroupBox();
                        skinGroupBox3.Location = groupBoxVideo3.Location;
                        skinGroupBox3.Size = groupBoxVideo3.Size;
                        //skinGroupBox1.Text = getValue.ToString();
                        skinGroupBox3.BorderColor = groupBoxVideo3.BorderColor;
                        skinGroupBox3.Text = tranceName;
                        skinGroupBox3.Tag = groupBoxVideo3.Tag;

                        skinPicture3.Location = VideoBox3.Location;
                        skinPicture3.Size = VideoBox3.Size;

                        SkinGroupBox groupboxPicEnter3 = new SkinGroupBox();
                        groupboxPicEnter3.Location = groupBoxVideoPic3_1.Location;
                        groupboxPicEnter3.Size = groupBoxVideoPic3_1.Size;
                        groupboxPicEnter3.Text = groupBoxVideoPic3_1.Text;
                        groupboxPicEnter3.Tag = groupBoxVideoPic3_1.Tag;

                        SkinGroupBox groupboxPicExit3 = new SkinGroupBox();
                        groupboxPicExit3.Location = groupBoxVideoPic3_2.Location;
                        groupboxPicExit3.Size = groupBoxVideoPic3_2.Size;
                        groupboxPicExit3.Text = groupBoxVideoPic3_2.Text;
                        groupboxPicExit3.Tag = groupBoxVideoPic3_2.Tag;

                        SkinButton SkinbuttonUP3 = new SkinButton();
                        SkinbuttonUP3.BaseColor = buttonUp3.BaseColor;
                        SkinbuttonUP3.Text = buttonUp3.Text;
                        SkinbuttonUP3.Size = buttonUp3.Size;
                        SkinbuttonUP3.Location = buttonUp3.Location;
                        SkinbuttonUP3.ForeColor = buttonUp3.ForeColor;
                        SkinbuttonUP3.Font = buttonUp3.Font;

                        skinGroupBox3.Controls.Add(SkinbuttonUP3);
                        skinGroupBox3.Controls.Add(skinPicture3);
                        skinGroupBox3.Controls.Add(groupboxPicEnter3);
                        skinGroupBox3.Controls.Add(groupboxPicExit3);
                        tabPage.Controls.Add(skinGroupBox3);
                        control = skinPicture3;
                        break;
                    case 3:
                        SkinPictureBox skinPicture4 = new SkinPictureBox();
                        SkinGroupBox skinGroupBox4 = new SkinGroupBox();
                        skinGroupBox4.Location = groupBoxVideo4.Location;
                        skinGroupBox4.Size = groupBoxVideo4.Size;
                        //skinGroupBox1.Text = getValue.ToString();
                        skinGroupBox4.BorderColor = groupBoxVideo4.BorderColor;
                        skinGroupBox4.Text = tranceName;
                        skinGroupBox4.Tag = groupBoxVideo4.Tag;

                        skinPicture4.Location = VideoBox4.Location;
                        skinPicture4.Size = VideoBox4.Size;

                        SkinGroupBox groupboxPicEnter4 = new SkinGroupBox();
                        groupboxPicEnter4.Location = groupBoxVideoPic4_1.Location;
                        groupboxPicEnter4.Size = groupBoxVideoPic4_1.Size;
                        groupboxPicEnter4.Text = groupBoxVideoPic4_1.Text;
                        groupboxPicEnter4.Tag = groupBoxVideoPic4_1.Tag;

                        SkinGroupBox groupboxPicExit4 = new SkinGroupBox();
                        groupboxPicExit4.Location = groupBoxVideoPic4_2.Location;
                        groupboxPicExit4.Size = groupBoxVideoPic4_2.Size;
                        groupboxPicExit4.Text = groupBoxVideoPic4_2.Text;
                        groupboxPicExit4.Tag = groupBoxVideoPic4_2.Tag;

                        SkinButton SkinbuttonUP4 = new SkinButton();
                        SkinbuttonUP4.BaseColor = buttonUp4.BaseColor;
                        SkinbuttonUP4.Text = buttonUp4.Text;
                        SkinbuttonUP4.Size = buttonUp4.Size;
                        SkinbuttonUP4.Location = buttonUp4.Location;
                        SkinbuttonUP4.ForeColor = buttonUp4.ForeColor;
                        SkinbuttonUP4.Font = buttonUp4.Font;

                        skinGroupBox4.Controls.Add(SkinbuttonUP4);
                        skinGroupBox4.Controls.Add(skinPicture4);
                        skinGroupBox4.Controls.Add(groupboxPicEnter4);
                        skinGroupBox4.Controls.Add(groupboxPicExit4);
                        tabPage.Controls.Add(skinGroupBox4);
                        control = skinPicture4;
                        break;
                }
            }

            return control;
        }
        #endregion
        #endregion

        #region 自适应
        private void FrmMain_SizeChanged(object sender, EventArgs e)
        {
            asf.controlAutoSize(this);
        }
        #endregion

        #region 触发方法

        #region 识别入场
        /// <summary>
        /// 识别入场
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void IPC_PlateEnterEvent(object sender, PlateEventArgs args)
        {
            string sightName = string.Empty, sightPath = string.Empty, plateName = string.Empty, platePath = string.Empty, imagebase64 = string.Empty;
            IPC ipc = (IPC)sender;

            //白牌车直接过
            if (args.PlateColor.GetDisplayName().Equals("白色"))
            {
                ipc.OpenGate();//打开道闸
                log.Debug("【抬杆命令：】白牌车入口识别抬杆，车牌号：" + args.PlateNum);
                return;
            }

            //只允许白名单车进入
            if (Params.Settings.EnabledWhiteList == true)
            {
                CheckLicenseTypeHelper.CheckWhiteList(args.PlateNum, 1, Convert.ToInt32(Params.Settings.ParkLot.Id));
            }
            ShowPictureVideoPic(ipc, false, args, ref sightName, ref sightPath, ref plateName, ref platePath, ref imagebase64);
        }
        #endregion

        #region 识别出场
        private void IPC_PlateExitEvent(object sender, PlateEventArgs args)
        {

        }
        #endregion

        private void IPC_StatusChangeEvent(object sender, DevStatusChangeEventArgs args)
        {
        }
        private void IPC_IOChangeEvent(object sender, DevIOChangeEventArgs args)
        {
        }
        private void on_plate(PlateEventArgs args, string ip, ref string sightName, ref string sightPath, ref string plateName, ref string platePath)
        {
        }
        #endregion

        #region 打开设置页
        private void skinButtonSetting_Click(object sender, EventArgs e)
        {

            ThreadSetting = new Thread(() =>
            {
                FrmSetting frmsetting = new FrmSetting();
                //frmsetting.TopMost = true;
                frmsetting.ShowDialog();
            });
            ThreadSetting.Start();
        }
        #endregion

        #region 各种按钮方法

        #region 按钮抬杆
        private void ButtonRaiseUp(object sender, EventArgs e)
        {
            SkinButton btnRaiseUp = (SkinButton)sender;
            string ip = btnRaiseUp.Tag.ToString();
            IPC iPC = mysdk.IPCs.FirstOrDefault(a => a.IP == ip);
            iPC.OpenGate();
        }
        #endregion
        #endregion

        #region 图片显示相关

        #region 显示图片
        /// <summary>
        /// 显示图片
        /// </summary>
        /// <param name="iPC"></param>
        /// <param name="isExit"></param>
        /// <param name="args"></param>
        /// <param name="sightName">抓拍的全景图名</param>
        /// <param name="sightPath">抓拍的全景图路径</param>
        /// <param name="plateName">抓拍的车牌局部图名</param>
        /// <param name="platePath">抓拍的车牌局部图路径</param>
        /// <param name="imagebase64">全景图base64</param>
        private void ShowPictureVideoPic(IPC iPC, bool isExit, PlateEventArgs args, ref string sightName, ref string sightPath, ref string plateName, ref string platePath, ref string imagebase64)
        {
            string ip = iPC.IP;

            Helper.ImageHelper.SaveCapPicture(args, ip, ref imagebase64, ref sightPath, ref sightName, ref platePath, ref plateName);
            Control groupBox = new Control();
            foreach (TabPage tabpage in tabControl1.TabPages)
            {
                groupBox = tabpage.Controls.Cast<Control>().FirstOrDefault(a => a.Tag.Equals(ip));
                if (groupBox != null)
                {
                    Control picControl = new Control();
                    Control ctl = new Control();
                    if (isExit == true)
                    {
                        ctl = groupBox.Controls.Cast<Control>().FirstOrDefault(a => a.Tag.Equals("Exit"));
                        picControl = ctl.Controls.Cast<Control>().FirstOrDefault(a => a.Tag.Equals("PictureExit"));
                        SkinPictureBox pictureBox = picControl as SkinPictureBox;
                        pictureBox.Load(platePath + "/" + plateName);
                    }
                    else
                    {
                        ctl = groupBox.Controls.Cast<Control>().FirstOrDefault(a => a.Tag.Equals("Enter"));
                        picControl = ctl.Controls.Cast<Control>().FirstOrDefault(a => a.Tag.Equals("PictureEnter"));
                        SkinPictureBox pictureBox = picControl as SkinPictureBox;
                        pictureBox.Load(sightPath + "/" + sightName);
                    }

                }
            }
        }

        #endregion

        #region 放大图片
        private void SetPictureBigger(object sender, EventArgs e)
        {
            try
            {
                SkinPictureBox pictureBox = sender as SkinPictureBox;
                string getUrl = pictureBox.ImageLocation;
                if (!string.IsNullOrEmpty(getUrl))
                {
                    FrmGetBiggerPic frmGetBiggerPic = new FrmGetBiggerPic(getUrl);
                    frmGetBiggerPic.ShowDialog();
                }
            }
            catch (Exception ex)
            {
            }
        }
        #endregion

        #endregion

        #region 显示屏相关

        #endregion

        #region 订单相关

        #endregion

    }
}
