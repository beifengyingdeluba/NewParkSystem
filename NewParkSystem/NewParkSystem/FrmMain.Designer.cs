﻿namespace NewParkSystem
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new CCWin.SkinControl.SkinTabControl();
            this.skinTabPage1 = new CCWin.SkinControl.SkinTabPage();
            this.groupBoxVideo3 = new CCWin.SkinControl.SkinGroupBox();
            this.buttonUp3 = new CCWin.SkinControl.SkinButton();
            this.groupBoxVideoPic3_1 = new CCWin.SkinControl.SkinGroupBox();
            this.videoPic3_1 = new CCWin.SkinControl.SkinPictureBox();
            this.groupBoxVideoPic3_2 = new CCWin.SkinControl.SkinGroupBox();
            this.videoPic3_2 = new CCWin.SkinControl.SkinPictureBox();
            this.VideoBox3 = new CCWin.SkinControl.SkinPictureBox();
            this.groupBoxVideo4 = new CCWin.SkinControl.SkinGroupBox();
            this.buttonUp4 = new CCWin.SkinControl.SkinButton();
            this.groupBoxVideoPic4_1 = new CCWin.SkinControl.SkinGroupBox();
            this.videoPic4_1 = new CCWin.SkinControl.SkinPictureBox();
            this.groupBoxVideoPic4_2 = new CCWin.SkinControl.SkinGroupBox();
            this.videoPic4_2 = new CCWin.SkinControl.SkinPictureBox();
            this.VideoBox4 = new CCWin.SkinControl.SkinPictureBox();
            this.groupBoxVideo1 = new CCWin.SkinControl.SkinGroupBox();
            this.groupBoxVideoPic1_1 = new CCWin.SkinControl.SkinGroupBox();
            this.videoPic1_1 = new CCWin.SkinControl.SkinPictureBox();
            this.buttonUp1 = new CCWin.SkinControl.SkinButton();
            this.groupBoxVideoPic1_2 = new CCWin.SkinControl.SkinGroupBox();
            this.videoPic1_2 = new CCWin.SkinControl.SkinPictureBox();
            this.VideoBox1 = new CCWin.SkinControl.SkinPictureBox();
            this.groupBoxVideo2 = new CCWin.SkinControl.SkinGroupBox();
            this.buttonUp2 = new CCWin.SkinControl.SkinButton();
            this.groupBoxVideoPic2_1 = new CCWin.SkinControl.SkinGroupBox();
            this.videoPic2_1 = new CCWin.SkinControl.SkinPictureBox();
            this.groupBoxVideoPic2_2 = new CCWin.SkinControl.SkinGroupBox();
            this.videoPic2_2 = new CCWin.SkinControl.SkinPictureBox();
            this.VideoBox2 = new CCWin.SkinControl.SkinPictureBox();
            this.groupBox1 = new CCWin.SkinControl.SkinGroupBox();
            this.skinGroupBox1 = new CCWin.SkinControl.SkinGroupBox();
            this.dataGradViewCarPass = new CCWin.SkinControl.SkinDataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbFee = new CCWin.SkinControl.SkinGroupBox();
            this.gbTollTaker = new CCWin.SkinControl.SkinGroupBox();
            this.gbOther = new CCWin.SkinControl.SkinGroupBox();
            this.skinButton11 = new CCWin.SkinControl.SkinButton();
            this.skinButton10 = new CCWin.SkinControl.SkinButton();
            this.skinButtonSetting = new CCWin.SkinControl.SkinButton();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tabControl1.SuspendLayout();
            this.skinTabPage1.SuspendLayout();
            this.groupBoxVideo3.SuspendLayout();
            this.groupBoxVideoPic3_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.videoPic3_1)).BeginInit();
            this.groupBoxVideoPic3_2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.videoPic3_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VideoBox3)).BeginInit();
            this.groupBoxVideo4.SuspendLayout();
            this.groupBoxVideoPic4_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.videoPic4_1)).BeginInit();
            this.groupBoxVideoPic4_2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.videoPic4_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VideoBox4)).BeginInit();
            this.groupBoxVideo1.SuspendLayout();
            this.groupBoxVideoPic1_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.videoPic1_1)).BeginInit();
            this.groupBoxVideoPic1_2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.videoPic1_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VideoBox1)).BeginInit();
            this.groupBoxVideo2.SuspendLayout();
            this.groupBoxVideoPic2_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.videoPic2_1)).BeginInit();
            this.groupBoxVideoPic2_2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.videoPic2_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VideoBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.skinGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGradViewCarPass)).BeginInit();
            this.gbOther.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl1.AnimatorType = CCWin.SkinControl.AnimationType.HorizSlide;
            this.tabControl1.CloseRect = new System.Drawing.Rectangle(2, 2, 12, 12);
            this.tabControl1.Controls.Add(this.skinTabPage1);
            this.tabControl1.HeadBack = null;
            this.tabControl1.ImgTxtOffset = new System.Drawing.Point(0, 0);
            this.tabControl1.ItemSize = new System.Drawing.Size(70, 36);
            this.tabControl1.Location = new System.Drawing.Point(7, 35);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.PageArrowDown = ((System.Drawing.Image)(resources.GetObject("tabControl1.PageArrowDown")));
            this.tabControl1.PageArrowHover = ((System.Drawing.Image)(resources.GetObject("tabControl1.PageArrowHover")));
            this.tabControl1.PageCloseHover = ((System.Drawing.Image)(resources.GetObject("tabControl1.PageCloseHover")));
            this.tabControl1.PageCloseNormal = ((System.Drawing.Image)(resources.GetObject("tabControl1.PageCloseNormal")));
            this.tabControl1.PageDown = ((System.Drawing.Image)(resources.GetObject("tabControl1.PageDown")));
            this.tabControl1.PageHover = ((System.Drawing.Image)(resources.GetObject("tabControl1.PageHover")));
            this.tabControl1.PageImagePosition = CCWin.SkinControl.SkinTabControl.ePageImagePosition.Left;
            this.tabControl1.PageNorml = null;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(991, 685);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 0;
            // 
            // skinTabPage1
            // 
            this.skinTabPage1.BackColor = System.Drawing.Color.White;
            this.skinTabPage1.Controls.Add(this.groupBoxVideo3);
            this.skinTabPage1.Controls.Add(this.groupBoxVideo4);
            this.skinTabPage1.Controls.Add(this.groupBoxVideo1);
            this.skinTabPage1.Controls.Add(this.groupBoxVideo2);
            this.skinTabPage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage1.Location = new System.Drawing.Point(0, 36);
            this.skinTabPage1.Name = "skinTabPage1";
            this.skinTabPage1.Size = new System.Drawing.Size(991, 649);
            this.skinTabPage1.TabIndex = 0;
            this.skinTabPage1.TabItemImage = null;
            this.skinTabPage1.Text = "TabPage1";
            // 
            // groupBoxVideo3
            // 
            this.groupBoxVideo3.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxVideo3.BorderColor = System.Drawing.Color.Silver;
            this.groupBoxVideo3.Controls.Add(this.buttonUp3);
            this.groupBoxVideo3.Controls.Add(this.groupBoxVideoPic3_1);
            this.groupBoxVideo3.Controls.Add(this.groupBoxVideoPic3_2);
            this.groupBoxVideo3.Controls.Add(this.VideoBox3);
            this.groupBoxVideo3.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBoxVideo3.ForeColor = System.Drawing.Color.Black;
            this.groupBoxVideo3.Location = new System.Drawing.Point(4, 328);
            this.groupBoxVideo3.Name = "groupBoxVideo3";
            this.groupBoxVideo3.RectBackColor = System.Drawing.Color.White;
            this.groupBoxVideo3.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.groupBoxVideo3.Size = new System.Drawing.Size(489, 319);
            this.groupBoxVideo3.TabIndex = 1;
            this.groupBoxVideo3.TabStop = false;
            this.groupBoxVideo3.TitleBorderColor = System.Drawing.Color.Black;
            this.groupBoxVideo3.TitleRectBackColor = System.Drawing.Color.White;
            this.groupBoxVideo3.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // buttonUp3
            // 
            this.buttonUp3.BackColor = System.Drawing.Color.Transparent;
            this.buttonUp3.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonUp3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonUp3.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.buttonUp3.DownBack = null;
            this.buttonUp3.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonUp3.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.buttonUp3.ForeColor = System.Drawing.Color.Black;
            this.buttonUp3.Location = new System.Drawing.Point(352, 234);
            this.buttonUp3.MouseBack = null;
            this.buttonUp3.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonUp3.Name = "buttonUp3";
            this.buttonUp3.NormlBack = null;
            this.buttonUp3.Size = new System.Drawing.Size(60, 28);
            this.buttonUp3.TabIndex = 4;
            this.buttonUp3.Text = "抬杆";
            this.buttonUp3.UseVisualStyleBackColor = false;
            // 
            // groupBoxVideoPic3_1
            // 
            this.groupBoxVideoPic3_1.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxVideoPic3_1.BorderColor = System.Drawing.Color.RoyalBlue;
            this.groupBoxVideoPic3_1.Controls.Add(this.videoPic3_1);
            this.groupBoxVideoPic3_1.ForeColor = System.Drawing.Color.Blue;
            this.groupBoxVideoPic3_1.Location = new System.Drawing.Point(2, 229);
            this.groupBoxVideoPic3_1.Name = "groupBoxVideoPic3_1";
            this.groupBoxVideoPic3_1.RectBackColor = System.Drawing.Color.White;
            this.groupBoxVideoPic3_1.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.groupBoxVideoPic3_1.Size = new System.Drawing.Size(172, 84);
            this.groupBoxVideoPic3_1.TabIndex = 5;
            this.groupBoxVideoPic3_1.TabStop = false;
            this.groupBoxVideoPic3_1.Tag = "Enter";
            this.groupBoxVideoPic3_1.Text = "入口";
            this.groupBoxVideoPic3_1.TitleBorderColor = System.Drawing.Color.White;
            this.groupBoxVideoPic3_1.TitleRectBackColor = System.Drawing.Color.White;
            this.groupBoxVideoPic3_1.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // videoPic3_1
            // 
            this.videoPic3_1.BackColor = System.Drawing.Color.Transparent;
            this.videoPic3_1.Location = new System.Drawing.Point(0, 12);
            this.videoPic3_1.Name = "videoPic3_1";
            this.videoPic3_1.Size = new System.Drawing.Size(172, 72);
            this.videoPic3_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.videoPic3_1.TabIndex = 4;
            this.videoPic3_1.TabStop = false;
            this.videoPic3_1.Tag = "PictureEnter";
            this.videoPic3_1.DoubleClick += new System.EventHandler(this.SetPictureBigger);
            // 
            // groupBoxVideoPic3_2
            // 
            this.groupBoxVideoPic3_2.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxVideoPic3_2.BorderColor = System.Drawing.Color.RoyalBlue;
            this.groupBoxVideoPic3_2.Controls.Add(this.videoPic3_2);
            this.groupBoxVideoPic3_2.ForeColor = System.Drawing.Color.Blue;
            this.groupBoxVideoPic3_2.Location = new System.Drawing.Point(180, 229);
            this.groupBoxVideoPic3_2.Name = "groupBoxVideoPic3_2";
            this.groupBoxVideoPic3_2.RectBackColor = System.Drawing.Color.White;
            this.groupBoxVideoPic3_2.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.groupBoxVideoPic3_2.Size = new System.Drawing.Size(165, 84);
            this.groupBoxVideoPic3_2.TabIndex = 6;
            this.groupBoxVideoPic3_2.TabStop = false;
            this.groupBoxVideoPic3_2.Tag = "Exit";
            this.groupBoxVideoPic3_2.Text = "出口";
            this.groupBoxVideoPic3_2.TitleBorderColor = System.Drawing.Color.White;
            this.groupBoxVideoPic3_2.TitleRectBackColor = System.Drawing.Color.White;
            this.groupBoxVideoPic3_2.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // videoPic3_2
            // 
            this.videoPic3_2.BackColor = System.Drawing.Color.Transparent;
            this.videoPic3_2.Location = new System.Drawing.Point(0, 12);
            this.videoPic3_2.Name = "videoPic3_2";
            this.videoPic3_2.Size = new System.Drawing.Size(166, 72);
            this.videoPic3_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.videoPic3_2.TabIndex = 5;
            this.videoPic3_2.TabStop = false;
            this.videoPic3_2.Tag = "PictureExit";
            this.videoPic3_2.DoubleClick += new System.EventHandler(this.SetPictureBigger);
            // 
            // VideoBox3
            // 
            this.VideoBox3.BackColor = System.Drawing.Color.Transparent;
            this.VideoBox3.Location = new System.Drawing.Point(0, 10);
            this.VideoBox3.Name = "VideoBox3";
            this.VideoBox3.Size = new System.Drawing.Size(489, 218);
            this.VideoBox3.TabIndex = 2;
            this.VideoBox3.TabStop = false;
            // 
            // groupBoxVideo4
            // 
            this.groupBoxVideo4.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxVideo4.BorderColor = System.Drawing.Color.Silver;
            this.groupBoxVideo4.Controls.Add(this.buttonUp4);
            this.groupBoxVideo4.Controls.Add(this.groupBoxVideoPic4_1);
            this.groupBoxVideo4.Controls.Add(this.groupBoxVideoPic4_2);
            this.groupBoxVideo4.Controls.Add(this.VideoBox4);
            this.groupBoxVideo4.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBoxVideo4.ForeColor = System.Drawing.Color.Black;
            this.groupBoxVideo4.Location = new System.Drawing.Point(499, 328);
            this.groupBoxVideo4.Name = "groupBoxVideo4";
            this.groupBoxVideo4.RectBackColor = System.Drawing.Color.White;
            this.groupBoxVideo4.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.groupBoxVideo4.Size = new System.Drawing.Size(489, 319);
            this.groupBoxVideo4.TabIndex = 2;
            this.groupBoxVideo4.TabStop = false;
            this.groupBoxVideo4.TitleBorderColor = System.Drawing.Color.Black;
            this.groupBoxVideo4.TitleRectBackColor = System.Drawing.Color.White;
            this.groupBoxVideo4.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // buttonUp4
            // 
            this.buttonUp4.BackColor = System.Drawing.Color.Transparent;
            this.buttonUp4.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonUp4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonUp4.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.buttonUp4.DownBack = null;
            this.buttonUp4.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonUp4.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.buttonUp4.ForeColor = System.Drawing.Color.Black;
            this.buttonUp4.Location = new System.Drawing.Point(358, 234);
            this.buttonUp4.MouseBack = null;
            this.buttonUp4.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonUp4.Name = "buttonUp4";
            this.buttonUp4.NormlBack = null;
            this.buttonUp4.Size = new System.Drawing.Size(67, 28);
            this.buttonUp4.TabIndex = 7;
            this.buttonUp4.Text = "抬杆";
            this.buttonUp4.UseVisualStyleBackColor = false;
            // 
            // groupBoxVideoPic4_1
            // 
            this.groupBoxVideoPic4_1.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxVideoPic4_1.BorderColor = System.Drawing.Color.RoyalBlue;
            this.groupBoxVideoPic4_1.Controls.Add(this.videoPic4_1);
            this.groupBoxVideoPic4_1.ForeColor = System.Drawing.Color.Blue;
            this.groupBoxVideoPic4_1.Location = new System.Drawing.Point(6, 229);
            this.groupBoxVideoPic4_1.Name = "groupBoxVideoPic4_1";
            this.groupBoxVideoPic4_1.RectBackColor = System.Drawing.Color.White;
            this.groupBoxVideoPic4_1.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.groupBoxVideoPic4_1.Size = new System.Drawing.Size(165, 84);
            this.groupBoxVideoPic4_1.TabIndex = 5;
            this.groupBoxVideoPic4_1.TabStop = false;
            this.groupBoxVideoPic4_1.Tag = "Enter";
            this.groupBoxVideoPic4_1.Text = "入口";
            this.groupBoxVideoPic4_1.TitleBorderColor = System.Drawing.Color.White;
            this.groupBoxVideoPic4_1.TitleRectBackColor = System.Drawing.Color.White;
            this.groupBoxVideoPic4_1.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // videoPic4_1
            // 
            this.videoPic4_1.BackColor = System.Drawing.Color.Transparent;
            this.videoPic4_1.Location = new System.Drawing.Point(-1, 12);
            this.videoPic4_1.Name = "videoPic4_1";
            this.videoPic4_1.Size = new System.Drawing.Size(166, 72);
            this.videoPic4_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.videoPic4_1.TabIndex = 6;
            this.videoPic4_1.TabStop = false;
            this.videoPic4_1.Tag = "PictureEnter";
            this.videoPic4_1.DoubleClick += new System.EventHandler(this.SetPictureBigger);
            // 
            // groupBoxVideoPic4_2
            // 
            this.groupBoxVideoPic4_2.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxVideoPic4_2.BorderColor = System.Drawing.Color.RoyalBlue;
            this.groupBoxVideoPic4_2.Controls.Add(this.videoPic4_2);
            this.groupBoxVideoPic4_2.ForeColor = System.Drawing.Color.Blue;
            this.groupBoxVideoPic4_2.Location = new System.Drawing.Point(186, 229);
            this.groupBoxVideoPic4_2.Name = "groupBoxVideoPic4_2";
            this.groupBoxVideoPic4_2.RectBackColor = System.Drawing.Color.White;
            this.groupBoxVideoPic4_2.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.groupBoxVideoPic4_2.Size = new System.Drawing.Size(165, 84);
            this.groupBoxVideoPic4_2.TabIndex = 6;
            this.groupBoxVideoPic4_2.TabStop = false;
            this.groupBoxVideoPic4_2.Tag = "Exit";
            this.groupBoxVideoPic4_2.Text = "出口";
            this.groupBoxVideoPic4_2.TitleBorderColor = System.Drawing.Color.White;
            this.groupBoxVideoPic4_2.TitleRectBackColor = System.Drawing.Color.White;
            this.groupBoxVideoPic4_2.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // videoPic4_2
            // 
            this.videoPic4_2.BackColor = System.Drawing.Color.Transparent;
            this.videoPic4_2.Location = new System.Drawing.Point(0, 12);
            this.videoPic4_2.Name = "videoPic4_2";
            this.videoPic4_2.Size = new System.Drawing.Size(166, 72);
            this.videoPic4_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.videoPic4_2.TabIndex = 7;
            this.videoPic4_2.TabStop = false;
            this.videoPic4_2.Tag = "PictureExit";
            this.videoPic4_2.DoubleClick += new System.EventHandler(this.SetPictureBigger);
            // 
            // VideoBox4
            // 
            this.VideoBox4.BackColor = System.Drawing.Color.Transparent;
            this.VideoBox4.Location = new System.Drawing.Point(0, 10);
            this.VideoBox4.Name = "VideoBox4";
            this.VideoBox4.Size = new System.Drawing.Size(483, 218);
            this.VideoBox4.TabIndex = 3;
            this.VideoBox4.TabStop = false;
            // 
            // groupBoxVideo1
            // 
            this.groupBoxVideo1.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxVideo1.BorderColor = System.Drawing.Color.Silver;
            this.groupBoxVideo1.Controls.Add(this.groupBoxVideoPic1_1);
            this.groupBoxVideo1.Controls.Add(this.buttonUp1);
            this.groupBoxVideo1.Controls.Add(this.groupBoxVideoPic1_2);
            this.groupBoxVideo1.Controls.Add(this.VideoBox1);
            this.groupBoxVideo1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBoxVideo1.ForeColor = System.Drawing.Color.Black;
            this.groupBoxVideo1.Location = new System.Drawing.Point(4, 3);
            this.groupBoxVideo1.Name = "groupBoxVideo1";
            this.groupBoxVideo1.RectBackColor = System.Drawing.Color.White;
            this.groupBoxVideo1.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.groupBoxVideo1.Size = new System.Drawing.Size(489, 319);
            this.groupBoxVideo1.TabIndex = 0;
            this.groupBoxVideo1.TabStop = false;
            this.groupBoxVideo1.TitleBorderColor = System.Drawing.Color.Black;
            this.groupBoxVideo1.TitleRectBackColor = System.Drawing.Color.White;
            this.groupBoxVideo1.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // groupBoxVideoPic1_1
            // 
            this.groupBoxVideoPic1_1.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxVideoPic1_1.BorderColor = System.Drawing.Color.RoyalBlue;
            this.groupBoxVideoPic1_1.Controls.Add(this.videoPic1_1);
            this.groupBoxVideoPic1_1.ForeColor = System.Drawing.Color.Blue;
            this.groupBoxVideoPic1_1.Location = new System.Drawing.Point(6, 229);
            this.groupBoxVideoPic1_1.Name = "groupBoxVideoPic1_1";
            this.groupBoxVideoPic1_1.RectBackColor = System.Drawing.Color.White;
            this.groupBoxVideoPic1_1.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.groupBoxVideoPic1_1.Size = new System.Drawing.Size(166, 84);
            this.groupBoxVideoPic1_1.TabIndex = 1;
            this.groupBoxVideoPic1_1.TabStop = false;
            this.groupBoxVideoPic1_1.Tag = "Enter";
            this.groupBoxVideoPic1_1.Text = "入口";
            this.groupBoxVideoPic1_1.TitleBorderColor = System.Drawing.Color.White;
            this.groupBoxVideoPic1_1.TitleRectBackColor = System.Drawing.Color.White;
            this.groupBoxVideoPic1_1.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // videoPic1_1
            // 
            this.videoPic1_1.BackColor = System.Drawing.Color.Transparent;
            this.videoPic1_1.Location = new System.Drawing.Point(0, 12);
            this.videoPic1_1.Name = "videoPic1_1";
            this.videoPic1_1.Size = new System.Drawing.Size(166, 72);
            this.videoPic1_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.videoPic1_1.TabIndex = 0;
            this.videoPic1_1.TabStop = false;
            this.videoPic1_1.Tag = "PictureEnter";
            this.videoPic1_1.DoubleClick += new System.EventHandler(this.SetPictureBigger);
            // 
            // buttonUp1
            // 
            this.buttonUp1.BackColor = System.Drawing.Color.Transparent;
            this.buttonUp1.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonUp1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonUp1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.buttonUp1.DownBack = null;
            this.buttonUp1.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonUp1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.buttonUp1.ForeColor = System.Drawing.Color.Black;
            this.buttonUp1.Location = new System.Drawing.Point(351, 235);
            this.buttonUp1.MouseBack = null;
            this.buttonUp1.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonUp1.Name = "buttonUp1";
            this.buttonUp1.NormlBack = null;
            this.buttonUp1.Size = new System.Drawing.Size(60, 27);
            this.buttonUp1.TabIndex = 3;
            this.buttonUp1.Text = "抬杆";
            this.buttonUp1.UseVisualStyleBackColor = false;
            this.buttonUp1.Click += new System.EventHandler(this.ButtonRaiseUp);
            // 
            // groupBoxVideoPic1_2
            // 
            this.groupBoxVideoPic1_2.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxVideoPic1_2.BorderColor = System.Drawing.Color.RoyalBlue;
            this.groupBoxVideoPic1_2.Controls.Add(this.videoPic1_2);
            this.groupBoxVideoPic1_2.ForeColor = System.Drawing.Color.Blue;
            this.groupBoxVideoPic1_2.Location = new System.Drawing.Point(180, 229);
            this.groupBoxVideoPic1_2.Name = "groupBoxVideoPic1_2";
            this.groupBoxVideoPic1_2.RectBackColor = System.Drawing.Color.White;
            this.groupBoxVideoPic1_2.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.groupBoxVideoPic1_2.Size = new System.Drawing.Size(165, 84);
            this.groupBoxVideoPic1_2.TabIndex = 2;
            this.groupBoxVideoPic1_2.TabStop = false;
            this.groupBoxVideoPic1_2.Tag = "Exit";
            this.groupBoxVideoPic1_2.Text = "出口";
            this.groupBoxVideoPic1_2.TitleBorderColor = System.Drawing.Color.White;
            this.groupBoxVideoPic1_2.TitleRectBackColor = System.Drawing.Color.White;
            this.groupBoxVideoPic1_2.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // videoPic1_2
            // 
            this.videoPic1_2.BackColor = System.Drawing.Color.Transparent;
            this.videoPic1_2.Location = new System.Drawing.Point(-1, 12);
            this.videoPic1_2.Name = "videoPic1_2";
            this.videoPic1_2.Size = new System.Drawing.Size(166, 72);
            this.videoPic1_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.videoPic1_2.TabIndex = 1;
            this.videoPic1_2.TabStop = false;
            this.videoPic1_2.Tag = "PictureExit";
            this.videoPic1_2.DoubleClick += new System.EventHandler(this.SetPictureBigger);
            // 
            // VideoBox1
            // 
            this.VideoBox1.BackColor = System.Drawing.Color.Transparent;
            this.VideoBox1.Location = new System.Drawing.Point(0, 11);
            this.VideoBox1.Name = "VideoBox1";
            this.VideoBox1.Size = new System.Drawing.Size(489, 218);
            this.VideoBox1.TabIndex = 0;
            this.VideoBox1.TabStop = false;
            // 
            // groupBoxVideo2
            // 
            this.groupBoxVideo2.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxVideo2.BorderColor = System.Drawing.Color.Silver;
            this.groupBoxVideo2.Controls.Add(this.buttonUp2);
            this.groupBoxVideo2.Controls.Add(this.groupBoxVideoPic2_1);
            this.groupBoxVideo2.Controls.Add(this.groupBoxVideoPic2_2);
            this.groupBoxVideo2.Controls.Add(this.VideoBox2);
            this.groupBoxVideo2.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBoxVideo2.ForeColor = System.Drawing.Color.Black;
            this.groupBoxVideo2.Location = new System.Drawing.Point(499, 3);
            this.groupBoxVideo2.Name = "groupBoxVideo2";
            this.groupBoxVideo2.RectBackColor = System.Drawing.Color.White;
            this.groupBoxVideo2.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.groupBoxVideo2.Size = new System.Drawing.Size(489, 319);
            this.groupBoxVideo2.TabIndex = 1;
            this.groupBoxVideo2.TabStop = false;
            this.groupBoxVideo2.TitleBorderColor = System.Drawing.Color.Black;
            this.groupBoxVideo2.TitleRectBackColor = System.Drawing.Color.White;
            this.groupBoxVideo2.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // buttonUp2
            // 
            this.buttonUp2.BackColor = System.Drawing.Color.Transparent;
            this.buttonUp2.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonUp2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonUp2.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.buttonUp2.DownBack = null;
            this.buttonUp2.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonUp2.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.buttonUp2.ForeColor = System.Drawing.Color.Black;
            this.buttonUp2.Location = new System.Drawing.Point(357, 235);
            this.buttonUp2.MouseBack = null;
            this.buttonUp2.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buttonUp2.Name = "buttonUp2";
            this.buttonUp2.NormlBack = null;
            this.buttonUp2.Size = new System.Drawing.Size(67, 27);
            this.buttonUp2.TabIndex = 4;
            this.buttonUp2.Text = "抬杆";
            this.buttonUp2.UseVisualStyleBackColor = false;
            // 
            // groupBoxVideoPic2_1
            // 
            this.groupBoxVideoPic2_1.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxVideoPic2_1.BorderColor = System.Drawing.Color.RoyalBlue;
            this.groupBoxVideoPic2_1.Controls.Add(this.videoPic2_1);
            this.groupBoxVideoPic2_1.ForeColor = System.Drawing.Color.Blue;
            this.groupBoxVideoPic2_1.Location = new System.Drawing.Point(6, 229);
            this.groupBoxVideoPic2_1.Name = "groupBoxVideoPic2_1";
            this.groupBoxVideoPic2_1.RectBackColor = System.Drawing.Color.White;
            this.groupBoxVideoPic2_1.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.groupBoxVideoPic2_1.Size = new System.Drawing.Size(165, 84);
            this.groupBoxVideoPic2_1.TabIndex = 3;
            this.groupBoxVideoPic2_1.TabStop = false;
            this.groupBoxVideoPic2_1.Tag = "Enter";
            this.groupBoxVideoPic2_1.Text = "入口";
            this.groupBoxVideoPic2_1.TitleBorderColor = System.Drawing.Color.White;
            this.groupBoxVideoPic2_1.TitleRectBackColor = System.Drawing.Color.White;
            this.groupBoxVideoPic2_1.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // videoPic2_1
            // 
            this.videoPic2_1.BackColor = System.Drawing.Color.Transparent;
            this.videoPic2_1.Location = new System.Drawing.Point(-1, 12);
            this.videoPic2_1.Name = "videoPic2_1";
            this.videoPic2_1.Size = new System.Drawing.Size(166, 72);
            this.videoPic2_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.videoPic2_1.TabIndex = 2;
            this.videoPic2_1.TabStop = false;
            this.videoPic2_1.Tag = "PictureEnter";
            this.videoPic2_1.DoubleClick += new System.EventHandler(this.SetPictureBigger);
            // 
            // groupBoxVideoPic2_2
            // 
            this.groupBoxVideoPic2_2.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxVideoPic2_2.BorderColor = System.Drawing.Color.RoyalBlue;
            this.groupBoxVideoPic2_2.Controls.Add(this.videoPic2_2);
            this.groupBoxVideoPic2_2.ForeColor = System.Drawing.Color.Blue;
            this.groupBoxVideoPic2_2.Location = new System.Drawing.Point(186, 229);
            this.groupBoxVideoPic2_2.Name = "groupBoxVideoPic2_2";
            this.groupBoxVideoPic2_2.RectBackColor = System.Drawing.Color.White;
            this.groupBoxVideoPic2_2.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.groupBoxVideoPic2_2.Size = new System.Drawing.Size(165, 84);
            this.groupBoxVideoPic2_2.TabIndex = 4;
            this.groupBoxVideoPic2_2.TabStop = false;
            this.groupBoxVideoPic2_2.Tag = "Exit";
            this.groupBoxVideoPic2_2.Text = "出口";
            this.groupBoxVideoPic2_2.TitleBorderColor = System.Drawing.Color.White;
            this.groupBoxVideoPic2_2.TitleRectBackColor = System.Drawing.Color.White;
            this.groupBoxVideoPic2_2.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // videoPic2_2
            // 
            this.videoPic2_2.BackColor = System.Drawing.Color.Transparent;
            this.videoPic2_2.Location = new System.Drawing.Point(-1, 12);
            this.videoPic2_2.Name = "videoPic2_2";
            this.videoPic2_2.Size = new System.Drawing.Size(166, 72);
            this.videoPic2_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.videoPic2_2.TabIndex = 3;
            this.videoPic2_2.TabStop = false;
            this.videoPic2_2.Tag = "PictureExit";
            this.videoPic2_2.DoubleClick += new System.EventHandler(this.SetPictureBigger);
            // 
            // VideoBox2
            // 
            this.VideoBox2.BackColor = System.Drawing.Color.Transparent;
            this.VideoBox2.Location = new System.Drawing.Point(0, 11);
            this.VideoBox2.Name = "VideoBox2";
            this.VideoBox2.Size = new System.Drawing.Size(483, 218);
            this.VideoBox2.TabIndex = 1;
            this.VideoBox2.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.BorderColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.skinGroupBox1);
            this.groupBox1.Controls.Add(this.gbFee);
            this.groupBox1.Controls.Add(this.gbTollTaker);
            this.groupBox1.Controls.Add(this.gbOther);
            this.groupBox1.ForeColor = System.Drawing.Color.Blue;
            this.groupBox1.Location = new System.Drawing.Point(1004, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RectBackColor = System.Drawing.Color.White;
            this.groupBox1.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.groupBox1.Size = new System.Drawing.Size(263, 672);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.TitleBorderColor = System.Drawing.Color.Red;
            this.groupBox1.TitleRectBackColor = System.Drawing.Color.White;
            this.groupBox1.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // skinGroupBox1
            // 
            this.skinGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.skinGroupBox1.BorderColor = System.Drawing.Color.DarkGray;
            this.skinGroupBox1.Controls.Add(this.dataGradViewCarPass);
            this.skinGroupBox1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold);
            this.skinGroupBox1.ForeColor = System.Drawing.Color.Black;
            this.skinGroupBox1.Location = new System.Drawing.Point(1, 230);
            this.skinGroupBox1.Name = "skinGroupBox1";
            this.skinGroupBox1.RectBackColor = System.Drawing.Color.White;
            this.skinGroupBox1.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinGroupBox1.Size = new System.Drawing.Size(262, 219);
            this.skinGroupBox1.TabIndex = 7;
            this.skinGroupBox1.TabStop = false;
            this.skinGroupBox1.Text = "过车记录";
            this.skinGroupBox1.TitleBorderColor = System.Drawing.Color.Transparent;
            this.skinGroupBox1.TitleRectBackColor = System.Drawing.Color.White;
            this.skinGroupBox1.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // dataGradViewCarPass
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(246)))), ((int)(((byte)(253)))));
            this.dataGradViewCarPass.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGradViewCarPass.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGradViewCarPass.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGradViewCarPass.ColumnFont = null;
            this.dataGradViewCarPass.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(246)))), ((int)(((byte)(239)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGradViewCarPass.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGradViewCarPass.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGradViewCarPass.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column3,
            this.Column4});
            this.dataGradViewCarPass.ColumnSelectForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(188)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGradViewCarPass.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGradViewCarPass.EnableHeadersVisualStyles = false;
            this.dataGradViewCarPass.GridColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.dataGradViewCarPass.HeadFont = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold);
            this.dataGradViewCarPass.HeadSelectForeColor = System.Drawing.SystemColors.HighlightText;
            this.dataGradViewCarPass.Location = new System.Drawing.Point(6, 15);
            this.dataGradViewCarPass.Name = "dataGradViewCarPass";
            this.dataGradViewCarPass.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGradViewCarPass.RowHeadersVisible = false;
            this.dataGradViewCarPass.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dataGradViewCarPass.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGradViewCarPass.RowTemplate.Height = 23;
            this.dataGradViewCarPass.Size = new System.Drawing.Size(250, 198);
            this.dataGradViewCarPass.TabIndex = 0;
            this.dataGradViewCarPass.TitleBack = null;
            this.dataGradViewCarPass.TitleBackColorBegin = System.Drawing.Color.White;
            this.dataGradViewCarPass.TitleBackColorEnd = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(196)))), ((int)(((byte)(242)))));
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column1.FillWeight = 30F;
            this.Column1.HeaderText = "车牌号";
            this.Column1.Name = "Column1";
            this.Column1.Width = 130;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column3.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column3.HeaderText = "时间";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column4.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column4.FillWeight = 150F;
            this.Column4.HeaderText = "出入口";
            this.Column4.Name = "Column4";
            this.Column4.Width = 130;
            // 
            // gbFee
            // 
            this.gbFee.BackColor = System.Drawing.Color.Transparent;
            this.gbFee.BorderColor = System.Drawing.Color.Silver;
            this.gbFee.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gbFee.ForeColor = System.Drawing.Color.Black;
            this.gbFee.Location = new System.Drawing.Point(1, 455);
            this.gbFee.Name = "gbFee";
            this.gbFee.RectBackColor = System.Drawing.Color.White;
            this.gbFee.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.gbFee.Size = new System.Drawing.Size(262, 217);
            this.gbFee.TabIndex = 6;
            this.gbFee.TabStop = false;
            this.gbFee.Text = "费用计算";
            this.gbFee.TitleBorderColor = System.Drawing.Color.Transparent;
            this.gbFee.TitleRectBackColor = System.Drawing.Color.White;
            this.gbFee.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // gbTollTaker
            // 
            this.gbTollTaker.BackColor = System.Drawing.Color.Transparent;
            this.gbTollTaker.BorderColor = System.Drawing.Color.Gainsboro;
            this.gbTollTaker.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold);
            this.gbTollTaker.ForeColor = System.Drawing.Color.Black;
            this.gbTollTaker.Location = new System.Drawing.Point(1, 104);
            this.gbTollTaker.Name = "gbTollTaker";
            this.gbTollTaker.RectBackColor = System.Drawing.Color.White;
            this.gbTollTaker.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.gbTollTaker.Size = new System.Drawing.Size(262, 120);
            this.gbTollTaker.TabIndex = 5;
            this.gbTollTaker.TabStop = false;
            this.gbTollTaker.Text = "收费员";
            this.gbTollTaker.TitleBorderColor = System.Drawing.Color.Transparent;
            this.gbTollTaker.TitleRectBackColor = System.Drawing.Color.White;
            this.gbTollTaker.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // gbOther
            // 
            this.gbOther.BackColor = System.Drawing.Color.Transparent;
            this.gbOther.BorderColor = System.Drawing.Color.Silver;
            this.gbOther.Controls.Add(this.skinButton11);
            this.gbOther.Controls.Add(this.skinButton10);
            this.gbOther.Controls.Add(this.skinButtonSetting);
            this.gbOther.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold);
            this.gbOther.ForeColor = System.Drawing.Color.Black;
            this.gbOther.Location = new System.Drawing.Point(1, 0);
            this.gbOther.Name = "gbOther";
            this.gbOther.RectBackColor = System.Drawing.Color.White;
            this.gbOther.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.gbOther.Size = new System.Drawing.Size(262, 98);
            this.gbOther.TabIndex = 4;
            this.gbOther.TabStop = false;
            this.gbOther.Text = "系统";
            this.gbOther.TitleBorderColor = System.Drawing.Color.Transparent;
            this.gbOther.TitleRectBackColor = System.Drawing.Color.White;
            this.gbOther.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // skinButton11
            // 
            this.skinButton11.BackColor = System.Drawing.Color.Transparent;
            this.skinButton11.BaseColor = System.Drawing.Color.PaleGreen;
            this.skinButton11.BorderColor = System.Drawing.Color.Transparent;
            this.skinButton11.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton11.DownBack = null;
            this.skinButton11.Location = new System.Drawing.Point(188, 25);
            this.skinButton11.MouseBack = null;
            this.skinButton11.Name = "skinButton11";
            this.skinButton11.NormlBack = null;
            this.skinButton11.Size = new System.Drawing.Size(68, 54);
            this.skinButton11.TabIndex = 2;
            this.skinButton11.Text = "同步数据";
            this.skinButton11.UseVisualStyleBackColor = false;
            // 
            // skinButton10
            // 
            this.skinButton10.BackColor = System.Drawing.Color.Transparent;
            this.skinButton10.BaseColor = System.Drawing.Color.Silver;
            this.skinButton10.BorderColor = System.Drawing.Color.Transparent;
            this.skinButton10.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton10.DownBack = null;
            this.skinButton10.Location = new System.Drawing.Point(99, 25);
            this.skinButton10.MouseBack = null;
            this.skinButton10.Name = "skinButton10";
            this.skinButton10.NormlBack = null;
            this.skinButton10.Size = new System.Drawing.Size(68, 54);
            this.skinButton10.TabIndex = 1;
            this.skinButton10.Text = "广告";
            this.skinButton10.UseVisualStyleBackColor = false;
            // 
            // skinButtonSetting
            // 
            this.skinButtonSetting.BackColor = System.Drawing.Color.Transparent;
            this.skinButtonSetting.BaseColor = System.Drawing.Color.Silver;
            this.skinButtonSetting.BorderColor = System.Drawing.Color.Transparent;
            this.skinButtonSetting.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButtonSetting.DownBack = null;
            this.skinButtonSetting.Location = new System.Drawing.Point(6, 25);
            this.skinButtonSetting.MouseBack = null;
            this.skinButtonSetting.Name = "skinButtonSetting";
            this.skinButtonSetting.NormlBack = null;
            this.skinButtonSetting.Size = new System.Drawing.Size(68, 54);
            this.skinButtonSetting.TabIndex = 0;
            this.skinButtonSetting.Text = "设置";
            this.skinButtonSetting.UseVisualStyleBackColor = false;
            this.skinButtonSetting.Click += new System.EventHandler(this.skinButtonSetting_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1274, 727);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.Text = "江南爱停车";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.SizeChanged += new System.EventHandler(this.FrmMain_SizeChanged);
            this.tabControl1.ResumeLayout(false);
            this.skinTabPage1.ResumeLayout(false);
            this.groupBoxVideo3.ResumeLayout(false);
            this.groupBoxVideoPic3_1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.videoPic3_1)).EndInit();
            this.groupBoxVideoPic3_2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.videoPic3_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VideoBox3)).EndInit();
            this.groupBoxVideo4.ResumeLayout(false);
            this.groupBoxVideoPic4_1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.videoPic4_1)).EndInit();
            this.groupBoxVideoPic4_2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.videoPic4_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VideoBox4)).EndInit();
            this.groupBoxVideo1.ResumeLayout(false);
            this.groupBoxVideoPic1_1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.videoPic1_1)).EndInit();
            this.groupBoxVideoPic1_2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.videoPic1_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VideoBox1)).EndInit();
            this.groupBoxVideo2.ResumeLayout(false);
            this.groupBoxVideoPic2_1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.videoPic2_1)).EndInit();
            this.groupBoxVideoPic2_2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.videoPic2_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VideoBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.skinGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGradViewCarPass)).EndInit();
            this.gbOther.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private CCWin.SkinControl.SkinTabControl tabControl1;
        private CCWin.SkinControl.SkinTabPage skinTabPage1;
        private CCWin.SkinControl.SkinGroupBox groupBox1;
        private CCWin.SkinControl.SkinGroupBox gbFee;
        private CCWin.SkinControl.SkinGroupBox gbTollTaker;
        private CCWin.SkinControl.SkinGroupBox gbOther;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private CCWin.SkinControl.SkinButton skinButton11;
        private CCWin.SkinControl.SkinButton skinButton10;
        private CCWin.SkinControl.SkinButton skinButtonSetting;
        private CCWin.SkinControl.SkinGroupBox groupBoxVideo4;
        private CCWin.SkinControl.SkinGroupBox groupBoxVideo2;
        private CCWin.SkinControl.SkinGroupBox groupBoxVideo1;
        private CCWin.SkinControl.SkinGroupBox groupBoxVideo3;
        private CCWin.SkinControl.SkinPictureBox VideoBox3;
        private CCWin.SkinControl.SkinPictureBox VideoBox4;
        private CCWin.SkinControl.SkinPictureBox VideoBox1;
        private CCWin.SkinControl.SkinPictureBox VideoBox2;
        private CCWin.SkinControl.SkinGroupBox groupBoxVideoPic1_1;
        private CCWin.SkinControl.SkinGroupBox groupBoxVideoPic3_1;
        private CCWin.SkinControl.SkinGroupBox groupBoxVideoPic3_2;
        private CCWin.SkinControl.SkinGroupBox groupBoxVideoPic4_1;
        private CCWin.SkinControl.SkinGroupBox groupBoxVideoPic4_2;
        private CCWin.SkinControl.SkinGroupBox groupBoxVideoPic1_2;
        private CCWin.SkinControl.SkinGroupBox groupBoxVideoPic2_1;
        private CCWin.SkinControl.SkinGroupBox groupBoxVideoPic2_2;
        private CCWin.SkinControl.SkinButton buttonUp3;
        private CCWin.SkinControl.SkinButton buttonUp4;
        private CCWin.SkinControl.SkinButton buttonUp1;
        private CCWin.SkinControl.SkinButton buttonUp2;
        private CCWin.SkinControl.SkinGroupBox skinGroupBox1;
        private CCWin.SkinControl.SkinDataGridView dataGradViewCarPass;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private CCWin.SkinControl.SkinPictureBox videoPic2_1;
        private CCWin.SkinControl.SkinPictureBox videoPic1_1;
        private CCWin.SkinControl.SkinPictureBox videoPic1_2;
        private CCWin.SkinControl.SkinPictureBox videoPic3_1;
        private CCWin.SkinControl.SkinPictureBox videoPic3_2;
        private CCWin.SkinControl.SkinPictureBox videoPic4_1;
        private CCWin.SkinControl.SkinPictureBox videoPic4_2;
        private CCWin.SkinControl.SkinPictureBox videoPic2_2;
    }
}