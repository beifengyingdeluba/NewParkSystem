﻿using Bealead.ICEIPC.Events;
using NewParkSystem.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewParkSystem.Helper
{
    public class ImageHelper
    {
        #region 保存图片
        public static void SaveCapPicture(PlateEventArgs args, string ip, ref string base64, ref string sightPath, ref string sightName, ref string platePath, ref string plateName)
        {
            if (args.SightImage != null && args.SightImage.Length > 0)
            {
                base64 = Convert.ToBase64String(args.SightImage);
                on_plate(args, ip, ref sightName, ref sightPath, ref plateName, ref platePath);
            }
            else
            {
                //log.Error("【抓拍异常】,车牌号:'" + args.PlateNum + "',未抓拍到图片，相机IP:" + ip);
            }
        }
        #endregion

        #region 区分全景图和识别图
        /// <summary>
        /// 区分全景图和识别图
        /// </summary>
        /// <param name="args"></param>
        private static void on_plate(PlateEventArgs args, string ip, ref string sightName, ref string sightPath, ref string plateName, ref string platePath)
        {
            //保存抓拍到的车牌全景图和车牌识别图
            StorePic(args.SightImage, ip, args.PlateNum, false, args.CaptureTime, ref sightName, ref sightPath);
            StorePic(args.PlateImage, ip, args.PlateNum, true, args.CaptureTime, ref plateName, ref platePath);
        }
        #endregion

        #region  保存出入场图片
        /// <summary>
        /// 保存出入场图片
        /// </summary>
        /// <param name="picData"></param>
        /// <param name="ip"></param>
        /// <param name="plateNumber"></param>
        /// <param name="isPlate"></param>
        /// <param name="capTime"></param>
        /// <param name="picSaveName"></param>
        /// <param name="picSavePath"></param>
        public static void StorePic(byte[] picData, string ip, string plateNumber, bool isPlate, DateTime capTime, ref string picSaveName, ref string picSavePath)
        {
            string dir = string.Format("{0}\\{1}\\{2}", Params.Settings.ImagePath, ip, capTime.ToString("yyyyMMdd"));
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            string picName = capTime.ToString("yyyyMMddHHmmss") + "_" + plateNumber;
            if (isPlate)
            {
                picName += "_plate";
            }
            picName += ".jpg";
            string picFullPath = dir + @"\" + picName;

            if (File.Exists(picFullPath))
            {
                int count = 1;
                while (count <= 10)
                {
                    picFullPath = dir + @"\" + capTime.ToString("yyyyMMddHHmmss") + "_" + plateNumber;
                    if (isPlate)
                    {
                        picFullPath += "_plate";
                    }
                    picFullPath += "_" + count.ToString() + ".jpg";

                    if (!File.Exists(picFullPath))
                    {
                        break;
                    }
                    count++;
                }
            }

            try
            {
                FileStream fs = new FileStream(picFullPath, FileMode.Create, FileAccess.Write);
                BinaryWriter bw = new BinaryWriter(fs);
                bw.Write(picData);
                bw.Close();
                fs.Close();
                picSaveName = picName;
                picSavePath = dir;
            }
            catch (System.Exception ex)
            {

            }
        }
        #endregion
    }
}
