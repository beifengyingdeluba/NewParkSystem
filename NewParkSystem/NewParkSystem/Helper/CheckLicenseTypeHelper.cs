﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewParkSystem.Helper
{
    public class CheckLicenseTypeHelper
    {
        #region 判断车辆类型相关

        #region 白名单
        /// <summary>
        /// 检查是否是白名单
        /// </summary>
        /// <param name="licensePlateNum">车牌号</param>
        /// <param name="licensePlateColor">车牌颜色</param>
        /// <param name="parkinglotId">停车场Id</param>
        /// <returns></returns>
        public static int CheckWhiteList(string licensePlateNum, int licensePlateColor, int parkinglotId)
        {
            int result = 0;
            return result;
        }
        #endregion

        #region 临时白名单
        /// <summary>
        /// 检查是否是临时白名单
        /// </summary>
        /// <param name="licensePlateNum">车牌号</param>
        /// <param name="licensePlateColor">车牌颜色</param>
        /// <param name="parkinglotId">停车场Id</param>
        /// <returns></returns>
        public static int CheckTmpWhiteList(string licensePlateNum, int licensePlateColor, int parkinglotId)
        {
            int result = 0;
            return result;
        }
        #endregion

        #region 黑名单
        /// <summary>
        /// 检查是否是黑名单
        /// </summary>
        /// <param name="licensePlateNum">车牌号</param>
        /// <param name="licensePlateColor">车牌颜色</param>
        /// <param name="parkinglotId">停车场Id</param>
        /// <returns></returns>
        public static int CheckBlackList(string licensePlateNum, int licensePlateColor, int parkinglotId)
        {
            int result = 0;
            return result;
        }
        #endregion

        #region 包月车
        /// <summary>
        /// 检查是否是包月车
        /// </summary>
        /// <param name="licensePlateNum">车牌号</param>
        /// <param name="licensePlateColor">车牌颜色</param>
        /// <param name="parkinglotId">停车场Id</param>
        /// <returns></returns>
        public static int CheckMonthlyList(string licensePlateNum, int licensePlateColor, int parkinglotId)
        {
            int result = 0;
            return result;
        }
        #endregion

        #region VIP
        /// <summary>
        /// 检查是否是包月车
        /// </summary>
        /// <param name="licensePlateNum">车牌号</param>
        /// <param name="licensePlateColor">车牌颜色</param>
        /// <param name="parkinglotId">停车场Id</param>
        /// <returns></returns>
        public static int CheckVIPList(string licensePlateNum, int licensePlateColor, int parkinglotId)
        {
            int result = 0;
            return result;
        }
        #endregion

        #endregion
    }
}
