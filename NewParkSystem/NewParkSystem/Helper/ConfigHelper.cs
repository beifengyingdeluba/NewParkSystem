﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using NewParkSystem.Models;
using NewParkSystem.Helper;

namespace NewParkSystem.Helper
{
    public class ConfigHelper<T> where T : new()
    {
        public static string dirPath = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
        public static string cfgFile = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\config.dat";

        //序列化操作
        public static void Serialize(T t, string fileName)
        {
            using (FileStream fs = new FileStream(fileName, FileMode.Create))
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, t);
                fs.Dispose();
            }
        }

        //反序列化操作
        public static T Deserialize(string fileName)
        {
            using (FileStream fs = new FileStream(fileName, FileMode.Open))
            {
                BinaryFormatter bf = new BinaryFormatter();
                T t = (T)bf.Deserialize(fs);
                fs.Dispose();
                return t;
            }
        }

        /// <summary>
        /// 载入配置
        /// </summary>
        /// <param name="setting"></param>
        public static void LoadConfig(ref Setting setting)
        {
            try
            {
                if (System.IO.File.Exists(cfgFile))
                {
                    setting = ConfigHelper<Setting>.Deserialize(cfgFile);
                }
                else
                {
                    setting = new Setting();
                    setting.ParkLot = new ParkingLot();
                    setting.ParkLot.Id = "0";
                    setting.ParkLot.No = "0";
                    setting.ParkLot.Name = "未命名";
                    setting.ParkLot.Addr = "";

                    Guard guard1 = new Guard();
                    guard1.IsExit = false;
                    guard1.No = "默认";
                    guard1.Primary = new GuardItem() { IP = "192.168.1.101", ScreenType = 1, ScreenIP = "192.168.1.101" };


                    Guard guard2 = new Guard();
                    guard2.IsExit = true;
                    guard2.No = "默认";
                    guard2.Primary = new GuardItem() { IP = "192.168.1.102", ScreenType = 1, ScreenIP = "192.168.1.102" };
                    setting.Guards = new List<Guard>() { guard1, guard2 };
                    setting.EnabledShowLeftCount = false;
                    setting.EnabledTmpWhiteList = false;
                    setting.EnabledWhiteListNoOrder = false;
                    setting.EnabledWhiteList = false;
                    setting.EnabledWLGO = false;
                    setting.ImagePath = @"D:\";
                    setting.InVolume = 10;
                    setting.OutVolume = 10;
                    setting.InDelay = 3000;
                    setting.OutDelay = 3000;
                    setting.InLine = 4;
                    setting.OutLine = 4;
                    setting.ScreenInDelay = 80;
                    setting.ScreenOutDelay = 80;
                    setting.EnableOneSpaceMoreCars = false;

                    setting.Serv = new Server()
                    {
                        HeartBeatFreq = 3,
                        Url = "http://47.100.229.60:8080",
                        HeartBeatMaxRetryCount = 5,
                        PayCheckFreq = 5000,
                        PayCheckMaxRetryCount = 25,
                        GateKeepOpenFreq = 3000,
                        DataSyncFreq = 600000
                    };
                    SaveConfig(setting);
                }
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// 保存配置
        /// </summary>
        /// <param name="setting"></param>
        public static void SaveConfig(Setting setting)
        {
            try
            {
                ConfigHelper<Setting>.Serialize(setting, cfgFile);
            }
            catch (Exception ex)
            {
            }
        }
    }
}
