﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewParkSystem.Models
{
    [Serializable]
    public class ParkingLot
    {
        public string Id { get; set; }
        //编号
        public string No { get; set; }
        //名称
        public string Name { get; set; }
        //类型
        public int PType { get; set; }
        //地址
        public string Addr { get; set; }
        //泊位数量
        public int BerthNum { get; set; }
        public string LedIds { get; set; }
        public bool IfLeds { get; set; }
        public string WebApiUrl { get; set; }
        public bool IfWebApiUrl { get; set; }
    }
}
