﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewParkSystem.Models
{
    [Serializable]
    public class Server
    {
        public string Url { get; set; }
        //心跳检测频率(秒)
        public int HeartBeatFreq { get; set; }
        //心跳检测最大失败次数
        public int HeartBeatMaxRetryCount { get; set; }
        //支付检查频率(秒)
        public int PayCheckFreq { get; set; }
        //支付检查最大次数
        public int PayCheckMaxRetryCount { get; set; }
        //道闸常开发送频率(秒)
        public int GateKeepOpenFreq { get; set; }
        //后台数据同步频率(分)
        public int DataSyncFreq { get; set; }
    }
}
