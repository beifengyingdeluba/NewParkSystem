﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewParkSystem.Models
{
    /// <summary>
    /// 设置类
    /// </summary>
    [Serializable]
    public class Setting
    {
        public ParkingLot ParkLot { get; set; }
        //出入口列表
        public List<Guard> Guards { get; set; }
        //图片路径保存目录
        public string ImagePath { get; set; }
        //入场屏幕音量
        public int InVolume { get; set; }
        //订单补录开关
        public bool EnabledRemakeOrder { get; set; }
        //出场屏幕音量
        public int OutVolume { get; set; }
        //入口多相机并发间隔时间(毫秒)
        public int InDelay { get; set; }
        //出口多相机并发间隔时间(毫秒)
        public int OutDelay { get; set; }
        public int ScreenInDelay { get; set; }
        public int ScreenOutDelay { get; set; }
        //入场显示屏行数
        public int InLine { get; set; }
        //出场显示屏行数
        public int OutLine { get; set; }
        public int CheckUnlicensePlate { get; set; }
        public Server Serv { get; set; }
        public bool EnableIfSign { get; set; }
        public bool EnabledWhiteList { get; set; }

        public bool EnabledVIPIn { get; set; }
        public bool EnableNoPlate { get; set; }
        public bool EnabledWhiteListNoOrder { get; set; }
        public bool EnabledWhiteListUsed { get; set; }
        public bool EnabledMonthlyPass { get; set; }
        public bool EnabledYellowReCalculation { get; set; }
        public bool EnabledFreeTime { get; set; }
        public bool EnabledShowBnt { get; set; }
        public bool EnabledWLGO { get; set; }
        public bool EnableOneSpaceMoreCars { get; set; }
        public bool EnabledShowLeftCount { get; set; }
        public bool EnabledBlackList { get; set; }
        public List<string> WhileList { get; set; }
        //出场未真正驶离，防止第二次抓拍间隔时间
        public int MistakenOutSec { get; set; }
        //入场单据延时误判处理间隔时间(分钟)
        public int MistakenEntranceMin { get; set; }
        public int HourSync { get; set; }
        public int MinSync { get; set; }
        //酒店白名单包月
        public bool EnabledTmpWhiteList { get; set; }
        //是否更新停车场泊位数
        public bool IsUpdateBerthNum { get; set; }
        //断网续传频率(秒)
        public int brokenNetOrderFre { get; set; }
        //是否显示播报欢迎光临
        public bool IsWelcome { get; set; }
        //接口超时时间
        public int apiTimeout { get; set; }
        //自动检测余位间隔
        public int CheckLeftCountSec { get; set; }
        //出场未识别检测时间间隔
        public int UnidentifiedTimerMin { get; set; }
    }
}
