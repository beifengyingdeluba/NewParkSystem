﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewParkSystem.Models
{
    public class Params
    {
        //默认无人值守的用户和员工Id
        public static readonly int noBodyUserId = 9999;
        public static readonly int noBodyEmpId = 9999;

        private static LoginUser _user = new LoginUser();
        public static LoginUser User { get; set; }
        public static ChargeOnDutyModel Duty { get; set; }
        public static Setting Settings { get; set; }
        public static string JsonConfig { get; set; }
    }
}
