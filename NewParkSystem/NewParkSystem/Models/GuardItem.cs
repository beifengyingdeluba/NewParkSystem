﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewParkSystem.Models
{
    [Serializable]
    public class GuardItem
    {
        //相机IP地址
        public string IP { get; set; }
        //屏幕类型
        public int ScreenType { get; set; }
        //屏幕IP地址
        public string ScreenIP { get; set; }
    }
}
