﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewParkSystem.Models
{
    /// <summary>
    /// 出入口实体类
    /// </summary>
    [Serializable]
    public class Guard
    {
        //编号或者名称
        public string No { get; set; }
        //是否是出口
        public bool IsExit { get; set; }
        //主相机
        public GuardItem Primary { get; set; }
        //副相机集合
        public List<GuardItem> Secondaries { get; set; }
    }
}
