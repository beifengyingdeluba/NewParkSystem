﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CCWin;
using CCWin.SkinClass;
using CCWin.SkinControl;

namespace NewParkSystem.Models
{
    public class VideoGroup
    {
        public string IP { get; set; }
        public SkinPictureBox pictureBoxEnter { get; set; }
        public SkinPictureBox pictureBoxExit { get; set; }
        public SkinPictureBox videoBox { get; set; }
        public SkinTextBox textBox { get; set; }
        public SkinButton buttonUp { get; set; }
        public string trance { get; set; }
    }
}
