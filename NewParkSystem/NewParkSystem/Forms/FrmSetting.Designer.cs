﻿namespace NewParkSystem.Forms
{
    partial class FrmSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSetting));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.skinTabControl1 = new CCWin.SkinControl.SkinTabControl();
            this.TabPageSetting = new CCWin.SkinControl.SkinTabPage();
            this.numericUpDown19 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown20 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown21 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown16 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown17 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown18 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown13 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown14 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown15 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown10 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown11 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown12 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown7 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown8 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown9 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown6 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.skinCheckBox1 = new CCWin.SkinControl.SkinCheckBox();
            this.skinTextBox2 = new CCWin.SkinControl.SkinTextBox();
            this.skinTextBox1 = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel23 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel22 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel19 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel20 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel21 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel16 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel17 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel18 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel15 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel14 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel13 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel10 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel11 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel12 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel7 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel8 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel9 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel4 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel5 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel6 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel3 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel2 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.btnBrowse = new CCWin.SkinControl.SkinButton();
            this.textBoxPicDir = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TabPageFunction = new CCWin.SkinControl.SkinTabPage();
            this.skinCheckBoxBlackList = new CCWin.SkinControl.SkinCheckBox();
            this.skinCheckBoxVIPList = new CCWin.SkinControl.SkinCheckBox();
            this.skinCheckBoxMonthly = new CCWin.SkinControl.SkinCheckBox();
            this.skinCheckBoxWhiteList = new CCWin.SkinControl.SkinCheckBox();
            this.skinCheckBoxTmpWhiteList = new CCWin.SkinControl.SkinCheckBox();
            this.skinCheckBoxIsWelcome = new CCWin.SkinControl.SkinCheckBox();
            this.skinTabPage1 = new CCWin.SkinControl.SkinTabPage();
            this.skinButtonEdit = new CCWin.SkinControl.SkinButton();
            this.skinButtonAddCamera = new CCWin.SkinControl.SkinButton();
            this.skinDataGridViewCamera = new CCWin.SkinControl.SkinDataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skinListBoxCamera = new CCWin.SkinControl.SkinListBox();
            this.btnSave = new CCWin.SkinControl.SkinButton();
            this.skinTabControl1.SuspendLayout();
            this.TabPageSetting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.TabPageFunction.SuspendLayout();
            this.skinTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skinDataGridViewCamera)).BeginInit();
            this.SuspendLayout();
            // 
            // skinTabControl1
            // 
            this.skinTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.skinTabControl1.AnimatorType = CCWin.SkinControl.AnimationType.HorizSlide;
            this.skinTabControl1.CloseRect = new System.Drawing.Rectangle(2, 2, 12, 12);
            this.skinTabControl1.Controls.Add(this.TabPageSetting);
            this.skinTabControl1.Controls.Add(this.TabPageFunction);
            this.skinTabControl1.Controls.Add(this.skinTabPage1);
            this.skinTabControl1.HeadBack = null;
            this.skinTabControl1.ImgTxtOffset = new System.Drawing.Point(0, 0);
            this.skinTabControl1.ItemSize = new System.Drawing.Size(70, 36);
            this.skinTabControl1.Location = new System.Drawing.Point(7, 30);
            this.skinTabControl1.Name = "skinTabControl1";
            this.skinTabControl1.PageArrowDown = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageArrowDown")));
            this.skinTabControl1.PageArrowHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageArrowHover")));
            this.skinTabControl1.PageCloseHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageCloseHover")));
            this.skinTabControl1.PageCloseNormal = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageCloseNormal")));
            this.skinTabControl1.PageDown = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageDown")));
            this.skinTabControl1.PageHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageHover")));
            this.skinTabControl1.PageImagePosition = CCWin.SkinControl.SkinTabControl.ePageImagePosition.Left;
            this.skinTabControl1.PageNorml = null;
            this.skinTabControl1.SelectedIndex = 0;
            this.skinTabControl1.Size = new System.Drawing.Size(568, 556);
            this.skinTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.skinTabControl1.TabIndex = 0;
            // 
            // TabPageSetting
            // 
            this.TabPageSetting.BackColor = System.Drawing.Color.White;
            this.TabPageSetting.Controls.Add(this.numericUpDown19);
            this.TabPageSetting.Controls.Add(this.numericUpDown20);
            this.TabPageSetting.Controls.Add(this.numericUpDown21);
            this.TabPageSetting.Controls.Add(this.numericUpDown16);
            this.TabPageSetting.Controls.Add(this.numericUpDown17);
            this.TabPageSetting.Controls.Add(this.numericUpDown18);
            this.TabPageSetting.Controls.Add(this.numericUpDown13);
            this.TabPageSetting.Controls.Add(this.numericUpDown14);
            this.TabPageSetting.Controls.Add(this.numericUpDown15);
            this.TabPageSetting.Controls.Add(this.numericUpDown10);
            this.TabPageSetting.Controls.Add(this.numericUpDown11);
            this.TabPageSetting.Controls.Add(this.numericUpDown12);
            this.TabPageSetting.Controls.Add(this.numericUpDown7);
            this.TabPageSetting.Controls.Add(this.numericUpDown8);
            this.TabPageSetting.Controls.Add(this.numericUpDown9);
            this.TabPageSetting.Controls.Add(this.numericUpDown4);
            this.TabPageSetting.Controls.Add(this.numericUpDown5);
            this.TabPageSetting.Controls.Add(this.numericUpDown6);
            this.TabPageSetting.Controls.Add(this.numericUpDown3);
            this.TabPageSetting.Controls.Add(this.numericUpDown2);
            this.TabPageSetting.Controls.Add(this.numericUpDown1);
            this.TabPageSetting.Controls.Add(this.skinCheckBox1);
            this.TabPageSetting.Controls.Add(this.skinTextBox2);
            this.TabPageSetting.Controls.Add(this.skinTextBox1);
            this.TabPageSetting.Controls.Add(this.skinLabel23);
            this.TabPageSetting.Controls.Add(this.skinLabel22);
            this.TabPageSetting.Controls.Add(this.skinLabel19);
            this.TabPageSetting.Controls.Add(this.skinLabel20);
            this.TabPageSetting.Controls.Add(this.skinLabel21);
            this.TabPageSetting.Controls.Add(this.skinLabel16);
            this.TabPageSetting.Controls.Add(this.skinLabel17);
            this.TabPageSetting.Controls.Add(this.skinLabel18);
            this.TabPageSetting.Controls.Add(this.skinLabel15);
            this.TabPageSetting.Controls.Add(this.skinLabel14);
            this.TabPageSetting.Controls.Add(this.skinLabel13);
            this.TabPageSetting.Controls.Add(this.skinLabel10);
            this.TabPageSetting.Controls.Add(this.skinLabel11);
            this.TabPageSetting.Controls.Add(this.skinLabel12);
            this.TabPageSetting.Controls.Add(this.skinLabel7);
            this.TabPageSetting.Controls.Add(this.skinLabel8);
            this.TabPageSetting.Controls.Add(this.skinLabel9);
            this.TabPageSetting.Controls.Add(this.skinLabel4);
            this.TabPageSetting.Controls.Add(this.skinLabel5);
            this.TabPageSetting.Controls.Add(this.skinLabel6);
            this.TabPageSetting.Controls.Add(this.skinLabel3);
            this.TabPageSetting.Controls.Add(this.skinLabel2);
            this.TabPageSetting.Controls.Add(this.skinLabel1);
            this.TabPageSetting.Controls.Add(this.btnBrowse);
            this.TabPageSetting.Controls.Add(this.textBoxPicDir);
            this.TabPageSetting.Controls.Add(this.label1);
            this.TabPageSetting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabPageSetting.Location = new System.Drawing.Point(0, 36);
            this.TabPageSetting.Name = "TabPageSetting";
            this.TabPageSetting.Size = new System.Drawing.Size(568, 520);
            this.TabPageSetting.TabIndex = 0;
            this.TabPageSetting.TabItemImage = null;
            this.TabPageSetting.Text = "设置";
            // 
            // numericUpDown19
            // 
            this.numericUpDown19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown19.Location = new System.Drawing.Point(464, 345);
            this.numericUpDown19.Name = "numericUpDown19";
            this.numericUpDown19.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown19.TabIndex = 49;
            // 
            // numericUpDown20
            // 
            this.numericUpDown20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown20.Location = new System.Drawing.Point(283, 345);
            this.numericUpDown20.Name = "numericUpDown20";
            this.numericUpDown20.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown20.TabIndex = 48;
            // 
            // numericUpDown21
            // 
            this.numericUpDown21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown21.Location = new System.Drawing.Point(109, 345);
            this.numericUpDown21.Name = "numericUpDown21";
            this.numericUpDown21.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown21.TabIndex = 47;
            // 
            // numericUpDown16
            // 
            this.numericUpDown16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown16.Location = new System.Drawing.Point(452, 300);
            this.numericUpDown16.Name = "numericUpDown16";
            this.numericUpDown16.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown16.TabIndex = 46;
            // 
            // numericUpDown17
            // 
            this.numericUpDown17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown17.Location = new System.Drawing.Point(295, 300);
            this.numericUpDown17.Name = "numericUpDown17";
            this.numericUpDown17.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown17.TabIndex = 45;
            // 
            // numericUpDown18
            // 
            this.numericUpDown18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown18.Location = new System.Drawing.Point(121, 300);
            this.numericUpDown18.Name = "numericUpDown18";
            this.numericUpDown18.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown18.TabIndex = 44;
            // 
            // numericUpDown13
            // 
            this.numericUpDown13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown13.Location = new System.Drawing.Point(440, 251);
            this.numericUpDown13.Name = "numericUpDown13";
            this.numericUpDown13.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown13.TabIndex = 43;
            // 
            // numericUpDown14
            // 
            this.numericUpDown14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown14.Location = new System.Drawing.Point(283, 251);
            this.numericUpDown14.Name = "numericUpDown14";
            this.numericUpDown14.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown14.TabIndex = 42;
            // 
            // numericUpDown15
            // 
            this.numericUpDown15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown15.Location = new System.Drawing.Point(109, 251);
            this.numericUpDown15.Name = "numericUpDown15";
            this.numericUpDown15.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown15.TabIndex = 41;
            // 
            // numericUpDown10
            // 
            this.numericUpDown10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown10.Location = new System.Drawing.Point(440, 206);
            this.numericUpDown10.Name = "numericUpDown10";
            this.numericUpDown10.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown10.TabIndex = 40;
            // 
            // numericUpDown11
            // 
            this.numericUpDown11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown11.Location = new System.Drawing.Point(283, 206);
            this.numericUpDown11.Name = "numericUpDown11";
            this.numericUpDown11.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown11.TabIndex = 39;
            // 
            // numericUpDown12
            // 
            this.numericUpDown12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown12.Location = new System.Drawing.Point(109, 206);
            this.numericUpDown12.Name = "numericUpDown12";
            this.numericUpDown12.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown12.TabIndex = 38;
            // 
            // numericUpDown7
            // 
            this.numericUpDown7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown7.Location = new System.Drawing.Point(440, 154);
            this.numericUpDown7.Name = "numericUpDown7";
            this.numericUpDown7.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown7.TabIndex = 37;
            // 
            // numericUpDown8
            // 
            this.numericUpDown8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown8.Location = new System.Drawing.Point(283, 154);
            this.numericUpDown8.Name = "numericUpDown8";
            this.numericUpDown8.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown8.TabIndex = 36;
            // 
            // numericUpDown9
            // 
            this.numericUpDown9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown9.Location = new System.Drawing.Point(109, 154);
            this.numericUpDown9.Name = "numericUpDown9";
            this.numericUpDown9.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown9.TabIndex = 35;
            // 
            // numericUpDown4
            // 
            this.numericUpDown4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown4.Location = new System.Drawing.Point(440, 99);
            this.numericUpDown4.Name = "numericUpDown4";
            this.numericUpDown4.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown4.TabIndex = 34;
            // 
            // numericUpDown5
            // 
            this.numericUpDown5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown5.Location = new System.Drawing.Point(283, 99);
            this.numericUpDown5.Name = "numericUpDown5";
            this.numericUpDown5.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown5.TabIndex = 33;
            // 
            // numericUpDown6
            // 
            this.numericUpDown6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown6.Location = new System.Drawing.Point(109, 99);
            this.numericUpDown6.Name = "numericUpDown6";
            this.numericUpDown6.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown6.TabIndex = 32;
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown3.Location = new System.Drawing.Point(440, 44);
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown3.TabIndex = 31;
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown2.Location = new System.Drawing.Point(283, 44);
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown2.TabIndex = 30;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown1.Location = new System.Drawing.Point(109, 44);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(56, 21);
            this.numericUpDown1.TabIndex = 29;
            // 
            // skinCheckBox1
            // 
            this.skinCheckBox1.AutoSize = true;
            this.skinCheckBox1.BackColor = System.Drawing.Color.Transparent;
            this.skinCheckBox1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinCheckBox1.DownBack = null;
            this.skinCheckBox1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinCheckBox1.Location = new System.Drawing.Point(308, 395);
            this.skinCheckBox1.MouseBack = null;
            this.skinCheckBox1.Name = "skinCheckBox1";
            this.skinCheckBox1.NormlBack = null;
            this.skinCheckBox1.SelectedDownBack = null;
            this.skinCheckBox1.SelectedMouseBack = null;
            this.skinCheckBox1.SelectedNormlBack = null;
            this.skinCheckBox1.Size = new System.Drawing.Size(123, 21);
            this.skinCheckBox1.TabIndex = 28;
            this.skinCheckBox1.Text = "更新停车场泊位数";
            this.skinCheckBox1.UseVisualStyleBackColor = false;
            // 
            // skinTextBox2
            // 
            this.skinTextBox2.BackColor = System.Drawing.Color.Transparent;
            this.skinTextBox2.DownBack = null;
            this.skinTextBox2.Icon = null;
            this.skinTextBox2.IconIsButton = false;
            this.skinTextBox2.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.skinTextBox2.IsPasswordChat = '\0';
            this.skinTextBox2.IsSystemPasswordChar = false;
            this.skinTextBox2.Lines = new string[0];
            this.skinTextBox2.Location = new System.Drawing.Point(96, 430);
            this.skinTextBox2.Margin = new System.Windows.Forms.Padding(0);
            this.skinTextBox2.MaxLength = 32767;
            this.skinTextBox2.MinimumSize = new System.Drawing.Size(28, 28);
            this.skinTextBox2.MouseBack = null;
            this.skinTextBox2.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.skinTextBox2.Multiline = false;
            this.skinTextBox2.Name = "skinTextBox2";
            this.skinTextBox2.NormlBack = null;
            this.skinTextBox2.Padding = new System.Windows.Forms.Padding(5);
            this.skinTextBox2.ReadOnly = false;
            this.skinTextBox2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.skinTextBox2.Size = new System.Drawing.Size(343, 28);
            // 
            // 
            // 
            this.skinTextBox2.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.skinTextBox2.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTextBox2.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.skinTextBox2.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.skinTextBox2.SkinTxt.Name = "BaseText";
            this.skinTextBox2.SkinTxt.Size = new System.Drawing.Size(333, 18);
            this.skinTextBox2.SkinTxt.TabIndex = 0;
            this.skinTextBox2.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.skinTextBox2.SkinTxt.WaterText = "";
            this.skinTextBox2.TabIndex = 27;
            this.skinTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.skinTextBox2.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.skinTextBox2.WaterText = "";
            this.skinTextBox2.WordWrap = true;
            // 
            // skinTextBox1
            // 
            this.skinTextBox1.BackColor = System.Drawing.Color.Transparent;
            this.skinTextBox1.DownBack = null;
            this.skinTextBox1.Icon = null;
            this.skinTextBox1.IconIsButton = false;
            this.skinTextBox1.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.skinTextBox1.IsPasswordChat = '\0';
            this.skinTextBox1.IsSystemPasswordChar = false;
            this.skinTextBox1.Lines = new string[0];
            this.skinTextBox1.Location = new System.Drawing.Point(96, 390);
            this.skinTextBox1.Margin = new System.Windows.Forms.Padding(0);
            this.skinTextBox1.MaxLength = 32767;
            this.skinTextBox1.MinimumSize = new System.Drawing.Size(28, 28);
            this.skinTextBox1.MouseBack = null;
            this.skinTextBox1.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.skinTextBox1.Multiline = true;
            this.skinTextBox1.Name = "skinTextBox1";
            this.skinTextBox1.NormlBack = null;
            this.skinTextBox1.Padding = new System.Windows.Forms.Padding(5);
            this.skinTextBox1.ReadOnly = false;
            this.skinTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.skinTextBox1.Size = new System.Drawing.Size(185, 29);
            // 
            // 
            // 
            this.skinTextBox1.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.skinTextBox1.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTextBox1.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.skinTextBox1.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.skinTextBox1.SkinTxt.Multiline = true;
            this.skinTextBox1.SkinTxt.Name = "BaseText";
            this.skinTextBox1.SkinTxt.Size = new System.Drawing.Size(175, 19);
            this.skinTextBox1.SkinTxt.TabIndex = 0;
            this.skinTextBox1.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.skinTextBox1.SkinTxt.WaterText = "";
            this.skinTextBox1.TabIndex = 26;
            this.skinTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.skinTextBox1.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.skinTextBox1.WaterText = "";
            this.skinTextBox1.WordWrap = true;
            // 
            // skinLabel23
            // 
            this.skinLabel23.AutoSize = true;
            this.skinLabel23.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel23.BorderColor = System.Drawing.Color.White;
            this.skinLabel23.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel23.Location = new System.Drawing.Point(25, 438);
            this.skinLabel23.Name = "skinLabel23";
            this.skinLabel23.Size = new System.Drawing.Size(56, 17);
            this.skinLabel23.TabIndex = 25;
            this.skinLabel23.Text = "后台地址";
            // 
            // skinLabel22
            // 
            this.skinLabel22.AutoSize = true;
            this.skinLabel22.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel22.BorderColor = System.Drawing.Color.White;
            this.skinLabel22.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel22.Location = new System.Drawing.Point(13, 395);
            this.skinLabel22.Name = "skinLabel22";
            this.skinLabel22.Size = new System.Drawing.Size(68, 17);
            this.skinLabel22.TabIndex = 24;
            this.skinLabel22.Text = "停车场编号";
            // 
            // skinLabel19
            // 
            this.skinLabel19.AutoSize = true;
            this.skinLabel19.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel19.BorderColor = System.Drawing.Color.White;
            this.skinLabel19.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel19.Location = new System.Drawing.Point(342, 349);
            this.skinLabel19.Name = "skinLabel19";
            this.skinLabel19.Size = new System.Drawing.Size(116, 17);
            this.skinLabel19.TabIndex = 23;
            this.skinLabel19.Text = "未识别订单处理间隔";
            // 
            // skinLabel20
            // 
            this.skinLabel20.AutoSize = true;
            this.skinLabel20.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel20.BorderColor = System.Drawing.Color.White;
            this.skinLabel20.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel20.Location = new System.Drawing.Point(190, 349);
            this.skinLabel20.Name = "skinLabel20";
            this.skinLabel20.Size = new System.Drawing.Size(56, 17);
            this.skinLabel20.TabIndex = 22;
            this.skinLabel20.Text = "心跳频率";
            // 
            // skinLabel21
            // 
            this.skinLabel21.AutoSize = true;
            this.skinLabel21.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel21.BorderColor = System.Drawing.Color.White;
            this.skinLabel21.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel21.Location = new System.Drawing.Point(10, 349);
            this.skinLabel21.Name = "skinLabel21";
            this.skinLabel21.Size = new System.Drawing.Size(80, 17);
            this.skinLabel21.TabIndex = 21;
            this.skinLabel21.Text = "心跳失败次数";
            // 
            // skinLabel16
            // 
            this.skinLabel16.AutoSize = true;
            this.skinLabel16.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel16.BorderColor = System.Drawing.Color.White;
            this.skinLabel16.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel16.Location = new System.Drawing.Point(359, 304);
            this.skinLabel16.Name = "skinLabel16";
            this.skinLabel16.Size = new System.Drawing.Size(80, 17);
            this.skinLabel16.TabIndex = 20;
            this.skinLabel16.Text = "支付检查频率";
            // 
            // skinLabel17
            // 
            this.skinLabel17.AutoSize = true;
            this.skinLabel17.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel17.BorderColor = System.Drawing.Color.White;
            this.skinLabel17.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel17.Location = new System.Drawing.Point(183, 304);
            this.skinLabel17.Name = "skinLabel17";
            this.skinLabel17.Size = new System.Drawing.Size(104, 17);
            this.skinLabel17.TabIndex = 19;
            this.skinLabel17.Text = "支付最大查询次数";
            // 
            // skinLabel18
            // 
            this.skinLabel18.AutoSize = true;
            this.skinLabel18.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel18.BorderColor = System.Drawing.Color.White;
            this.skinLabel18.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel18.Location = new System.Drawing.Point(10, 304);
            this.skinLabel18.Name = "skinLabel18";
            this.skinLabel18.Size = new System.Drawing.Size(104, 17);
            this.skinLabel18.TabIndex = 18;
            this.skinLabel18.Text = "道闸常开发送频率";
            // 
            // skinLabel15
            // 
            this.skinLabel15.AutoSize = true;
            this.skinLabel15.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel15.BorderColor = System.Drawing.Color.White;
            this.skinLabel15.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel15.Location = new System.Drawing.Point(359, 255);
            this.skinLabel15.Name = "skinLabel15";
            this.skinLabel15.Size = new System.Drawing.Size(68, 17);
            this.skinLabel15.TabIndex = 17;
            this.skinLabel15.Text = "未驶离误判";
            // 
            // skinLabel14
            // 
            this.skinLabel14.AutoSize = true;
            this.skinLabel14.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel14.BorderColor = System.Drawing.Color.White;
            this.skinLabel14.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel14.Location = new System.Drawing.Point(190, 255);
            this.skinLabel14.Name = "skinLabel14";
            this.skinLabel14.Size = new System.Drawing.Size(80, 17);
            this.skinLabel14.TabIndex = 16;
            this.skinLabel14.Text = "入场延时误判";
            // 
            // skinLabel13
            // 
            this.skinLabel13.AutoSize = true;
            this.skinLabel13.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel13.BorderColor = System.Drawing.Color.White;
            this.skinLabel13.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel13.Location = new System.Drawing.Point(10, 255);
            this.skinLabel13.Name = "skinLabel13";
            this.skinLabel13.Size = new System.Drawing.Size(80, 17);
            this.skinLabel13.TabIndex = 15;
            this.skinLabel13.Text = "自动同步时间";
            // 
            // skinLabel10
            // 
            this.skinLabel10.AutoSize = true;
            this.skinLabel10.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel10.BorderColor = System.Drawing.Color.White;
            this.skinLabel10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel10.Location = new System.Drawing.Point(356, 206);
            this.skinLabel10.Name = "skinLabel10";
            this.skinLabel10.Size = new System.Drawing.Size(80, 17);
            this.skinLabel10.TabIndex = 14;
            this.skinLabel10.Text = "数据同步频率";
            // 
            // skinLabel11
            // 
            this.skinLabel11.AutoSize = true;
            this.skinLabel11.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel11.BorderColor = System.Drawing.Color.White;
            this.skinLabel11.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel11.Location = new System.Drawing.Point(191, 206);
            this.skinLabel11.Name = "skinLabel11";
            this.skinLabel11.Size = new System.Drawing.Size(88, 17);
            this.skinLabel11.TabIndex = 13;
            this.skinLabel11.Text = "屏幕延迟(出场)";
            // 
            // skinLabel12
            // 
            this.skinLabel12.AutoSize = true;
            this.skinLabel12.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel12.BorderColor = System.Drawing.Color.White;
            this.skinLabel12.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel12.Location = new System.Drawing.Point(8, 206);
            this.skinLabel12.Name = "skinLabel12";
            this.skinLabel12.Size = new System.Drawing.Size(88, 17);
            this.skinLabel12.TabIndex = 12;
            this.skinLabel12.Text = "屏幕延迟(入场)";
            // 
            // skinLabel7
            // 
            this.skinLabel7.AutoSize = true;
            this.skinLabel7.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel7.BorderColor = System.Drawing.Color.White;
            this.skinLabel7.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel7.Location = new System.Drawing.Point(355, 154);
            this.skinLabel7.Name = "skinLabel7";
            this.skinLabel7.Size = new System.Drawing.Size(80, 17);
            this.skinLabel7.TabIndex = 11;
            this.skinLabel7.Text = "断网续传频率";
            // 
            // skinLabel8
            // 
            this.skinLabel8.AutoSize = true;
            this.skinLabel8.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel8.BorderColor = System.Drawing.Color.White;
            this.skinLabel8.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel8.Location = new System.Drawing.Point(190, 154);
            this.skinLabel8.Name = "skinLabel8";
            this.skinLabel8.Size = new System.Drawing.Size(88, 17);
            this.skinLabel8.TabIndex = 10;
            this.skinLabel8.Text = "屏幕行数(出场)";
            // 
            // skinLabel9
            // 
            this.skinLabel9.AutoSize = true;
            this.skinLabel9.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel9.BorderColor = System.Drawing.Color.White;
            this.skinLabel9.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel9.Location = new System.Drawing.Point(7, 154);
            this.skinLabel9.Name = "skinLabel9";
            this.skinLabel9.Size = new System.Drawing.Size(88, 17);
            this.skinLabel9.TabIndex = 9;
            this.skinLabel9.Text = "屏幕行数(入场)";
            // 
            // skinLabel4
            // 
            this.skinLabel4.AutoSize = true;
            this.skinLabel4.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel4.BorderColor = System.Drawing.Color.White;
            this.skinLabel4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel4.Location = new System.Drawing.Point(354, 103);
            this.skinLabel4.Name = "skinLabel4";
            this.skinLabel4.Size = new System.Drawing.Size(80, 17);
            this.skinLabel4.TabIndex = 8;
            this.skinLabel4.Text = "接口超时时间";
            // 
            // skinLabel5
            // 
            this.skinLabel5.AutoSize = true;
            this.skinLabel5.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel5.BorderColor = System.Drawing.Color.White;
            this.skinLabel5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel5.Location = new System.Drawing.Point(189, 103);
            this.skinLabel5.Name = "skinLabel5";
            this.skinLabel5.Size = new System.Drawing.Size(88, 17);
            this.skinLabel5.TabIndex = 7;
            this.skinLabel5.Text = "屏幕音量(入场)";
            // 
            // skinLabel6
            // 
            this.skinLabel6.AutoSize = true;
            this.skinLabel6.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel6.BorderColor = System.Drawing.Color.White;
            this.skinLabel6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel6.Location = new System.Drawing.Point(6, 103);
            this.skinLabel6.Name = "skinLabel6";
            this.skinLabel6.Size = new System.Drawing.Size(88, 17);
            this.skinLabel6.TabIndex = 6;
            this.skinLabel6.Text = "屏幕音量(入场)";
            // 
            // skinLabel3
            // 
            this.skinLabel3.AutoSize = true;
            this.skinLabel3.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel3.BorderColor = System.Drawing.Color.White;
            this.skinLabel3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel3.Location = new System.Drawing.Point(354, 48);
            this.skinLabel3.Name = "skinLabel3";
            this.skinLabel3.Size = new System.Drawing.Size(80, 17);
            this.skinLabel3.TabIndex = 5;
            this.skinLabel3.Text = "余位查询间隔";
            // 
            // skinLabel2
            // 
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.BorderColor = System.Drawing.Color.White;
            this.skinLabel2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel2.Location = new System.Drawing.Point(189, 48);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(88, 17);
            this.skinLabel2.TabIndex = 4;
            this.skinLabel2.Text = "相机延迟(出场)";
            // 
            // skinLabel1
            // 
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.BorderColor = System.Drawing.Color.White;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel1.Location = new System.Drawing.Point(6, 48);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(88, 17);
            this.skinLabel1.TabIndex = 3;
            this.skinLabel1.Text = "相机延迟(入场)";
            // 
            // btnBrowse
            // 
            this.btnBrowse.BackColor = System.Drawing.Color.Transparent;
            this.btnBrowse.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnBrowse.BorderColor = System.Drawing.Color.Transparent;
            this.btnBrowse.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btnBrowse.DownBack = null;
            this.btnBrowse.Location = new System.Drawing.Point(440, 8);
            this.btnBrowse.MouseBack = null;
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.NormlBack = null;
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 2;
            this.btnBrowse.Text = "浏览";
            this.btnBrowse.UseVisualStyleBackColor = false;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // textBoxPicDir
            // 
            this.textBoxPicDir.Location = new System.Drawing.Point(96, 8);
            this.textBoxPicDir.Name = "textBoxPicDir";
            this.textBoxPicDir.Size = new System.Drawing.Size(327, 21);
            this.textBoxPicDir.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(7, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "图片保存目录:";
            // 
            // TabPageFunction
            // 
            this.TabPageFunction.BackColor = System.Drawing.Color.White;
            this.TabPageFunction.Controls.Add(this.skinCheckBoxBlackList);
            this.TabPageFunction.Controls.Add(this.skinCheckBoxVIPList);
            this.TabPageFunction.Controls.Add(this.skinCheckBoxMonthly);
            this.TabPageFunction.Controls.Add(this.skinCheckBoxWhiteList);
            this.TabPageFunction.Controls.Add(this.skinCheckBoxTmpWhiteList);
            this.TabPageFunction.Controls.Add(this.skinCheckBoxIsWelcome);
            this.TabPageFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabPageFunction.Location = new System.Drawing.Point(0, 36);
            this.TabPageFunction.Name = "TabPageFunction";
            this.TabPageFunction.Size = new System.Drawing.Size(568, 520);
            this.TabPageFunction.TabIndex = 1;
            this.TabPageFunction.TabItemImage = null;
            this.TabPageFunction.Text = "功能";
            // 
            // skinCheckBoxBlackList
            // 
            this.skinCheckBoxBlackList.AutoSize = true;
            this.skinCheckBoxBlackList.BackColor = System.Drawing.Color.Transparent;
            this.skinCheckBoxBlackList.ControlState = CCWin.SkinClass.ControlState.Focused;
            this.skinCheckBoxBlackList.DownBack = null;
            this.skinCheckBoxBlackList.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinCheckBoxBlackList.Location = new System.Drawing.Point(3, 138);
            this.skinCheckBoxBlackList.MouseBack = null;
            this.skinCheckBoxBlackList.Name = "skinCheckBoxBlackList";
            this.skinCheckBoxBlackList.NormlBack = null;
            this.skinCheckBoxBlackList.SelectedDownBack = null;
            this.skinCheckBoxBlackList.SelectedMouseBack = null;
            this.skinCheckBoxBlackList.SelectedNormlBack = null;
            this.skinCheckBoxBlackList.Size = new System.Drawing.Size(63, 21);
            this.skinCheckBoxBlackList.TabIndex = 6;
            this.skinCheckBoxBlackList.Text = "黑名单";
            this.skinCheckBoxBlackList.UseVisualStyleBackColor = false;
            // 
            // skinCheckBoxVIPList
            // 
            this.skinCheckBoxVIPList.AutoSize = true;
            this.skinCheckBoxVIPList.BackColor = System.Drawing.Color.Transparent;
            this.skinCheckBoxVIPList.ControlState = CCWin.SkinClass.ControlState.Focused;
            this.skinCheckBoxVIPList.DownBack = null;
            this.skinCheckBoxVIPList.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinCheckBoxVIPList.Location = new System.Drawing.Point(3, 111);
            this.skinCheckBoxVIPList.MouseBack = null;
            this.skinCheckBoxVIPList.Name = "skinCheckBoxVIPList";
            this.skinCheckBoxVIPList.NormlBack = null;
            this.skinCheckBoxVIPList.SelectedDownBack = null;
            this.skinCheckBoxVIPList.SelectedMouseBack = null;
            this.skinCheckBoxVIPList.SelectedNormlBack = null;
            this.skinCheckBoxVIPList.Size = new System.Drawing.Size(58, 21);
            this.skinCheckBoxVIPList.TabIndex = 5;
            this.skinCheckBoxVIPList.Text = "VIP车";
            this.skinCheckBoxVIPList.UseVisualStyleBackColor = false;
            // 
            // skinCheckBoxMonthly
            // 
            this.skinCheckBoxMonthly.AutoSize = true;
            this.skinCheckBoxMonthly.BackColor = System.Drawing.Color.Transparent;
            this.skinCheckBoxMonthly.ControlState = CCWin.SkinClass.ControlState.Focused;
            this.skinCheckBoxMonthly.DownBack = null;
            this.skinCheckBoxMonthly.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinCheckBoxMonthly.Location = new System.Drawing.Point(3, 84);
            this.skinCheckBoxMonthly.MouseBack = null;
            this.skinCheckBoxMonthly.Name = "skinCheckBoxMonthly";
            this.skinCheckBoxMonthly.NormlBack = null;
            this.skinCheckBoxMonthly.SelectedDownBack = null;
            this.skinCheckBoxMonthly.SelectedMouseBack = null;
            this.skinCheckBoxMonthly.SelectedNormlBack = null;
            this.skinCheckBoxMonthly.Size = new System.Drawing.Size(63, 21);
            this.skinCheckBoxMonthly.TabIndex = 4;
            this.skinCheckBoxMonthly.Text = "包月车";
            this.skinCheckBoxMonthly.UseVisualStyleBackColor = false;
            // 
            // skinCheckBoxWhiteList
            // 
            this.skinCheckBoxWhiteList.AutoSize = true;
            this.skinCheckBoxWhiteList.BackColor = System.Drawing.Color.Transparent;
            this.skinCheckBoxWhiteList.ControlState = CCWin.SkinClass.ControlState.Focused;
            this.skinCheckBoxWhiteList.DownBack = null;
            this.skinCheckBoxWhiteList.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinCheckBoxWhiteList.Location = new System.Drawing.Point(3, 57);
            this.skinCheckBoxWhiteList.MouseBack = null;
            this.skinCheckBoxWhiteList.Name = "skinCheckBoxWhiteList";
            this.skinCheckBoxWhiteList.NormlBack = null;
            this.skinCheckBoxWhiteList.SelectedDownBack = null;
            this.skinCheckBoxWhiteList.SelectedMouseBack = null;
            this.skinCheckBoxWhiteList.SelectedNormlBack = null;
            this.skinCheckBoxWhiteList.Size = new System.Drawing.Size(63, 21);
            this.skinCheckBoxWhiteList.TabIndex = 3;
            this.skinCheckBoxWhiteList.Text = "白名单";
            this.skinCheckBoxWhiteList.UseVisualStyleBackColor = false;
            // 
            // skinCheckBoxTmpWhiteList
            // 
            this.skinCheckBoxTmpWhiteList.AutoSize = true;
            this.skinCheckBoxTmpWhiteList.BackColor = System.Drawing.Color.Transparent;
            this.skinCheckBoxTmpWhiteList.ControlState = CCWin.SkinClass.ControlState.Focused;
            this.skinCheckBoxTmpWhiteList.DownBack = null;
            this.skinCheckBoxTmpWhiteList.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinCheckBoxTmpWhiteList.Location = new System.Drawing.Point(3, 30);
            this.skinCheckBoxTmpWhiteList.MouseBack = null;
            this.skinCheckBoxTmpWhiteList.Name = "skinCheckBoxTmpWhiteList";
            this.skinCheckBoxTmpWhiteList.NormlBack = null;
            this.skinCheckBoxTmpWhiteList.SelectedDownBack = null;
            this.skinCheckBoxTmpWhiteList.SelectedMouseBack = null;
            this.skinCheckBoxTmpWhiteList.SelectedNormlBack = null;
            this.skinCheckBoxTmpWhiteList.Size = new System.Drawing.Size(87, 21);
            this.skinCheckBoxTmpWhiteList.TabIndex = 2;
            this.skinCheckBoxTmpWhiteList.Text = "临时白名单";
            this.skinCheckBoxTmpWhiteList.UseVisualStyleBackColor = false;
            // 
            // skinCheckBoxIsWelcome
            // 
            this.skinCheckBoxIsWelcome.AutoSize = true;
            this.skinCheckBoxIsWelcome.BackColor = System.Drawing.Color.Transparent;
            this.skinCheckBoxIsWelcome.ControlState = CCWin.SkinClass.ControlState.Focused;
            this.skinCheckBoxIsWelcome.DownBack = null;
            this.skinCheckBoxIsWelcome.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinCheckBoxIsWelcome.Location = new System.Drawing.Point(3, 3);
            this.skinCheckBoxIsWelcome.MouseBack = null;
            this.skinCheckBoxIsWelcome.Name = "skinCheckBoxIsWelcome";
            this.skinCheckBoxIsWelcome.NormlBack = null;
            this.skinCheckBoxIsWelcome.SelectedDownBack = null;
            this.skinCheckBoxIsWelcome.SelectedMouseBack = null;
            this.skinCheckBoxIsWelcome.SelectedNormlBack = null;
            this.skinCheckBoxIsWelcome.Size = new System.Drawing.Size(99, 21);
            this.skinCheckBoxIsWelcome.TabIndex = 1;
            this.skinCheckBoxIsWelcome.Text = "播报欢迎光临";
            this.skinCheckBoxIsWelcome.UseVisualStyleBackColor = false;
            // 
            // skinTabPage1
            // 
            this.skinTabPage1.BackColor = System.Drawing.Color.White;
            this.skinTabPage1.Controls.Add(this.skinButtonEdit);
            this.skinTabPage1.Controls.Add(this.skinButtonAddCamera);
            this.skinTabPage1.Controls.Add(this.skinDataGridViewCamera);
            this.skinTabPage1.Controls.Add(this.skinListBoxCamera);
            this.skinTabPage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage1.Location = new System.Drawing.Point(0, 36);
            this.skinTabPage1.Name = "skinTabPage1";
            this.skinTabPage1.Size = new System.Drawing.Size(568, 520);
            this.skinTabPage1.TabIndex = 2;
            this.skinTabPage1.TabItemImage = null;
            this.skinTabPage1.Text = "相机设置";
            // 
            // skinButtonEdit
            // 
            this.skinButtonEdit.BackColor = System.Drawing.Color.Transparent;
            this.skinButtonEdit.BaseColor = System.Drawing.Color.Gray;
            this.skinButtonEdit.BorderColor = System.Drawing.Color.Silver;
            this.skinButtonEdit.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButtonEdit.DownBack = null;
            this.skinButtonEdit.DownBaseColor = System.Drawing.Color.Silver;
            this.skinButtonEdit.Location = new System.Drawing.Point(161, 360);
            this.skinButtonEdit.MouseBack = null;
            this.skinButtonEdit.MouseBaseColor = System.Drawing.Color.Gray;
            this.skinButtonEdit.Name = "skinButtonEdit";
            this.skinButtonEdit.NormlBack = null;
            this.skinButtonEdit.Size = new System.Drawing.Size(75, 23);
            this.skinButtonEdit.TabIndex = 3;
            this.skinButtonEdit.Text = "添加相机";
            this.skinButtonEdit.UseVisualStyleBackColor = false;
            // 
            // skinButtonAddCamera
            // 
            this.skinButtonAddCamera.BackColor = System.Drawing.Color.Transparent;
            this.skinButtonAddCamera.BaseColor = System.Drawing.Color.Gray;
            this.skinButtonAddCamera.BorderColor = System.Drawing.Color.Silver;
            this.skinButtonAddCamera.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButtonAddCamera.DownBack = null;
            this.skinButtonAddCamera.DownBaseColor = System.Drawing.Color.Silver;
            this.skinButtonAddCamera.Location = new System.Drawing.Point(56, 360);
            this.skinButtonAddCamera.MouseBack = null;
            this.skinButtonAddCamera.MouseBaseColor = System.Drawing.Color.Gray;
            this.skinButtonAddCamera.Name = "skinButtonAddCamera";
            this.skinButtonAddCamera.NormlBack = null;
            this.skinButtonAddCamera.Size = new System.Drawing.Size(75, 23);
            this.skinButtonAddCamera.TabIndex = 2;
            this.skinButtonAddCamera.Text = "添加相机";
            this.skinButtonAddCamera.UseVisualStyleBackColor = false;
            this.skinButtonAddCamera.Click += new System.EventHandler(this.skinButtonAddCamera_Click);
            // 
            // skinDataGridViewCamera
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(246)))), ((int)(((byte)(253)))));
            this.skinDataGridViewCamera.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.skinDataGridViewCamera.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.skinDataGridViewCamera.BackgroundColor = System.Drawing.SystemColors.Window;
            this.skinDataGridViewCamera.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.skinDataGridViewCamera.ColumnFont = null;
            this.skinDataGridViewCamera.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(246)))), ((int)(((byte)(239)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.skinDataGridViewCamera.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.skinDataGridViewCamera.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.skinDataGridViewCamera.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.skinDataGridViewCamera.ColumnSelectForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(188)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.skinDataGridViewCamera.DefaultCellStyle = dataGridViewCellStyle3;
            this.skinDataGridViewCamera.EnableHeadersVisualStyles = false;
            this.skinDataGridViewCamera.GridColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.skinDataGridViewCamera.HeadFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinDataGridViewCamera.HeadSelectForeColor = System.Drawing.SystemColors.HighlightText;
            this.skinDataGridViewCamera.Location = new System.Drawing.Point(130, 0);
            this.skinDataGridViewCamera.Name = "skinDataGridViewCamera";
            this.skinDataGridViewCamera.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.skinDataGridViewCamera.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.skinDataGridViewCamera.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.skinDataGridViewCamera.RowTemplate.Height = 23;
            this.skinDataGridViewCamera.Size = new System.Drawing.Size(438, 324);
            this.skinDataGridViewCamera.TabIndex = 1;
            this.skinDataGridViewCamera.TitleBack = null;
            this.skinDataGridViewCamera.TitleBackColorBegin = System.Drawing.Color.White;
            this.skinDataGridViewCamera.TitleBackColorEnd = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(196)))), ((int)(((byte)(242)))));
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.HeaderText = "主相机";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.HeaderText = "相机IP";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column3.HeaderText = "屏幕类型";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column4.HeaderText = "屏幕IP";
            this.Column4.Name = "Column4";
            // 
            // skinListBoxCamera
            // 
            this.skinListBoxCamera.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.skinListBoxCamera.Back = null;
            this.skinListBoxCamera.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.skinListBoxCamera.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinListBoxCamera.FormattingEnabled = true;
            this.skinListBoxCamera.ItemHeight = 20;
            this.skinListBoxCamera.Location = new System.Drawing.Point(0, 0);
            this.skinListBoxCamera.MouseColor = System.Drawing.Color.LightSteelBlue;
            this.skinListBoxCamera.Name = "skinListBoxCamera";
            this.skinListBoxCamera.RowBackColor2 = System.Drawing.Color.White;
            this.skinListBoxCamera.Size = new System.Drawing.Size(131, 324);
            this.skinListBoxCamera.TabIndex = 0;
            this.skinListBoxCamera.SelectedIndexChanged += new System.EventHandler(this.skinListBoxCamera_SelectedIndexChanged);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Transparent;
            this.btnSave.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSave.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSave.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btnSave.DownBack = null;
            this.btnSave.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSave.Location = new System.Drawing.Point(465, 609);
            this.btnSave.MouseBack = null;
            this.btnSave.Name = "btnSave";
            this.btnSave.NormlBack = null;
            this.btnSave.Size = new System.Drawing.Size(75, 53);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FrmSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 689);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.skinTabControl1);
            this.Name = "FrmSetting";
            this.ShowDrawIcon = false;
            this.ShowIcon = false;
            this.Text = "参数设置";
            this.Load += new System.EventHandler(this.FrmSetting_Load);
            this.skinTabControl1.ResumeLayout(false);
            this.TabPageSetting.ResumeLayout(false);
            this.TabPageSetting.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.TabPageFunction.ResumeLayout(false);
            this.TabPageFunction.PerformLayout();
            this.skinTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.skinDataGridViewCamera)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CCWin.SkinControl.SkinTabControl skinTabControl1;
        private CCWin.SkinControl.SkinTabPage TabPageSetting;
        private CCWin.SkinControl.SkinTabPage TabPageFunction;
        private CCWin.SkinControl.SkinButton btnSave;
        private System.Windows.Forms.TextBox textBoxPicDir;
        private System.Windows.Forms.Label label1;
        private CCWin.SkinControl.SkinButton btnBrowse;
        private CCWin.SkinControl.SkinCheckBox skinCheckBox1;
        private CCWin.SkinControl.SkinTextBox skinTextBox2;
        private CCWin.SkinControl.SkinTextBox skinTextBox1;
        private CCWin.SkinControl.SkinLabel skinLabel23;
        private CCWin.SkinControl.SkinLabel skinLabel22;
        private CCWin.SkinControl.SkinLabel skinLabel19;
        private CCWin.SkinControl.SkinLabel skinLabel20;
        private CCWin.SkinControl.SkinLabel skinLabel21;
        private CCWin.SkinControl.SkinLabel skinLabel16;
        private CCWin.SkinControl.SkinLabel skinLabel17;
        private CCWin.SkinControl.SkinLabel skinLabel18;
        private CCWin.SkinControl.SkinLabel skinLabel15;
        private CCWin.SkinControl.SkinLabel skinLabel14;
        private CCWin.SkinControl.SkinLabel skinLabel13;
        private CCWin.SkinControl.SkinLabel skinLabel10;
        private CCWin.SkinControl.SkinLabel skinLabel11;
        private CCWin.SkinControl.SkinLabel skinLabel12;
        private CCWin.SkinControl.SkinLabel skinLabel7;
        private CCWin.SkinControl.SkinLabel skinLabel8;
        private CCWin.SkinControl.SkinLabel skinLabel9;
        private CCWin.SkinControl.SkinLabel skinLabel4;
        private CCWin.SkinControl.SkinLabel skinLabel5;
        private CCWin.SkinControl.SkinLabel skinLabel6;
        private CCWin.SkinControl.SkinLabel skinLabel3;
        private CCWin.SkinControl.SkinLabel skinLabel2;
        private CCWin.SkinControl.SkinLabel skinLabel1;
        private System.Windows.Forms.NumericUpDown numericUpDown19;
        private System.Windows.Forms.NumericUpDown numericUpDown20;
        private System.Windows.Forms.NumericUpDown numericUpDown21;
        private System.Windows.Forms.NumericUpDown numericUpDown16;
        private System.Windows.Forms.NumericUpDown numericUpDown17;
        private System.Windows.Forms.NumericUpDown numericUpDown18;
        private System.Windows.Forms.NumericUpDown numericUpDown13;
        private System.Windows.Forms.NumericUpDown numericUpDown14;
        private System.Windows.Forms.NumericUpDown numericUpDown15;
        private System.Windows.Forms.NumericUpDown numericUpDown10;
        private System.Windows.Forms.NumericUpDown numericUpDown11;
        private System.Windows.Forms.NumericUpDown numericUpDown12;
        private System.Windows.Forms.NumericUpDown numericUpDown7;
        private System.Windows.Forms.NumericUpDown numericUpDown8;
        private System.Windows.Forms.NumericUpDown numericUpDown9;
        private System.Windows.Forms.NumericUpDown numericUpDown4;
        private System.Windows.Forms.NumericUpDown numericUpDown5;
        private System.Windows.Forms.NumericUpDown numericUpDown6;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private CCWin.SkinControl.SkinCheckBox skinCheckBoxIsWelcome;
        private CCWin.SkinControl.SkinCheckBox skinCheckBoxWhiteList;
        private CCWin.SkinControl.SkinCheckBox skinCheckBoxTmpWhiteList;
        private CCWin.SkinControl.SkinCheckBox skinCheckBoxMonthly;
        private CCWin.SkinControl.SkinCheckBox skinCheckBoxBlackList;
        private CCWin.SkinControl.SkinCheckBox skinCheckBoxVIPList;
        private CCWin.SkinControl.SkinTabPage skinTabPage1;
        private CCWin.SkinControl.SkinListBox skinListBoxCamera;
        private CCWin.SkinControl.SkinDataGridView skinDataGridViewCamera;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private CCWin.SkinControl.SkinButton skinButtonAddCamera;
        private CCWin.SkinControl.SkinButton skinButtonEdit;
    }
}