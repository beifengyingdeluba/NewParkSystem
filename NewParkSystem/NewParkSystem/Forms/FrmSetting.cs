﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CCWin;
using NewParkSystem.Helper;
using NewParkSystem.Models;
using Sql;
using System.Threading;
using System.IO;

namespace NewParkSystem.Forms
{
    public partial class FrmSetting : Skin_Mac
    {
        #region 变量
        private Thread SetGuardThread = null;
        #endregion
        public FrmSetting()
        {
            InitializeComponent();
        }

        private void FrmSetting_Load(object sender, EventArgs e)
        {
            this.MaximizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            ReadSetting();
            skinTabControl1.SelectedIndex = 0;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveSetting();
                ConfigHelper<Setting>.SaveConfig(Params.Settings);
                MessageBox.Show("保存成功");
                this.Close();
            }
            catch (Exception ex)
            {
            }

        }

        private void ReadSetting()
        {
            #region Bool
            skinCheckBoxIsWelcome.Checked = Params.Settings.IsWelcome;
            skinCheckBoxTmpWhiteList.Checked = Params.Settings.EnabledTmpWhiteList;
            skinCheckBoxWhiteList.Checked = Params.Settings.EnabledWhiteList;
            skinCheckBoxMonthly.Checked = Params.Settings.EnabledMonthlyPass;
            skinCheckBoxVIPList.Checked = Params.Settings.EnabledVIPIn;
            skinCheckBoxBlackList.Checked = Params.Settings.EnabledBlackList;
            #endregion

            #region 数值
            textBoxPicDir.Text = Params.Settings.ImagePath;
            #endregion

            #region 载入相机
            LoadCameraConfig();
            #endregion
        }

        private void LoadCameraConfig()
        {
            if (Params.Settings.Guards != null)
            {
                foreach (var item in Params.Settings.Guards)
                {
                    skinListBoxCamera.Items.Add(new CCWin.SkinControl.SkinListBoxItem(item.No + "_" + (item.IsExit ? "入口" : "出口")));
                }
            }
        }

        private void SaveSetting()
        {
            #region Bool

            Params.Settings.IsWelcome = skinCheckBoxIsWelcome.Checked;
            Params.Settings.EnabledTmpWhiteList = skinCheckBoxTmpWhiteList.Checked;
            Params.Settings.EnabledWhiteList = skinCheckBoxWhiteList.Checked;
            Params.Settings.EnabledMonthlyPass = skinCheckBoxMonthly.Checked;
            Params.Settings.EnabledVIPIn = skinCheckBoxVIPList.Checked;
            Params.Settings.EnabledBlackList = skinCheckBoxBlackList.Checked;
            #endregion

            #region 数值
            Params.Settings.ImagePath = textBoxPicDir.Text.Trim();
            #endregion
        }

        private void skinButtonAddCamera_Click(object sender, EventArgs e)
        {
            SetGuardThread = new Thread(() =>
            {
                try
                {
                    FrmGuard frmGuard = new FrmGuard();
                    frmGuard.TopMost = true;
                    frmGuard.ShowDialog();
                }
                catch (Exception ex)
                {
                }
            });
            SetGuardThread.Start();
        }

        private void skinListBoxCamera_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Guard guard = new Guard();
                string selectedCamsera = string.Empty;
                selectedCamsera = skinListBoxCamera.SelectedItem.ToString().Split('_')[0];
                guard = Params.Settings.Guards.First(a => a.No == selectedCamsera);
                skinDataGridViewCamera.Rows.Clear();
                skinDataGridViewCamera.Rows.Add(1);
                skinDataGridViewCamera.Rows[0].Cells[0].Value = guard.No;
                skinDataGridViewCamera.Rows[0].Cells[1].Value = guard.Primary.IP;
                skinDataGridViewCamera.Rows[0].Cells[2].Value = "";
                skinDataGridViewCamera.Rows[0].Cells[3].Value = guard.Primary.ScreenIP;
            }
            catch (Exception ex)
            {

            }

        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(new ThreadStart(PictureDialog));
            thread.SetApartmentState(ApartmentState.STA); //重点
            thread.Start();
        }

        private void PictureDialog()   //打开一个选择图片的对话框
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                SetText(dialog.SelectedPath);
            }
        }

        #region 用委托修改textBoxPicDir的值
        delegate void SetTextCallback(string text);
        public void SetText(string text)
        {
            if (this.textBoxPicDir.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.textBoxPicDir.Text = text;
            }
        }
        #endregion
    }
}
