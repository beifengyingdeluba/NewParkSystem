﻿namespace NewParkSystem.Forms
{
    partial class FrmGuard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.skinRadioButton1 = new CCWin.SkinControl.SkinRadioButton();
            this.skinRadioButton2 = new CCWin.SkinControl.SkinRadioButton();
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel2 = new CCWin.SkinControl.SkinLabel();
            this.skinTextBoxGuardName = new CCWin.SkinControl.SkinTextBox();
            this.skinTextBoxIpAddress = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel3 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel4 = new CCWin.SkinControl.SkinLabel();
            this.skinRadioButtonOtherCamera = new CCWin.SkinControl.SkinRadioButton();
            this.skinRadioButtonMainCamera = new CCWin.SkinControl.SkinRadioButton();
            this.skinTextBoxScreenIP = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel5 = new CCWin.SkinControl.SkinLabel();
            this.skinButtonSave = new CCWin.SkinControl.SkinButton();
            this.skinButtonCancel = new CCWin.SkinControl.SkinButton();
            this.SuspendLayout();
            // 
            // skinRadioButton1
            // 
            this.skinRadioButton1.AutoSize = true;
            this.skinRadioButton1.BackColor = System.Drawing.Color.Transparent;
            this.skinRadioButton1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinRadioButton1.DownBack = null;
            this.skinRadioButton1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinRadioButton1.Location = new System.Drawing.Point(109, 57);
            this.skinRadioButton1.MouseBack = null;
            this.skinRadioButton1.Name = "skinRadioButton1";
            this.skinRadioButton1.NormlBack = null;
            this.skinRadioButton1.SelectedDownBack = null;
            this.skinRadioButton1.SelectedMouseBack = null;
            this.skinRadioButton1.SelectedNormlBack = null;
            this.skinRadioButton1.Size = new System.Drawing.Size(50, 21);
            this.skinRadioButton1.TabIndex = 0;
            this.skinRadioButton1.TabStop = true;
            this.skinRadioButton1.Text = "入口";
            this.skinRadioButton1.UseVisualStyleBackColor = false;
            // 
            // skinRadioButton2
            // 
            this.skinRadioButton2.AutoSize = true;
            this.skinRadioButton2.BackColor = System.Drawing.Color.Transparent;
            this.skinRadioButton2.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinRadioButton2.DownBack = null;
            this.skinRadioButton2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinRadioButton2.Location = new System.Drawing.Point(178, 57);
            this.skinRadioButton2.MouseBack = null;
            this.skinRadioButton2.Name = "skinRadioButton2";
            this.skinRadioButton2.NormlBack = null;
            this.skinRadioButton2.SelectedDownBack = null;
            this.skinRadioButton2.SelectedMouseBack = null;
            this.skinRadioButton2.SelectedNormlBack = null;
            this.skinRadioButton2.Size = new System.Drawing.Size(50, 21);
            this.skinRadioButton2.TabIndex = 1;
            this.skinRadioButton2.TabStop = true;
            this.skinRadioButton2.Text = "出口";
            this.skinRadioButton2.UseVisualStyleBackColor = false;
            // 
            // skinLabel1
            // 
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.BorderColor = System.Drawing.Color.White;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel1.Location = new System.Drawing.Point(22, 59);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(68, 17);
            this.skinLabel1.TabIndex = 2;
            this.skinLabel1.Text = "出入口类型";
            // 
            // skinLabel2
            // 
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.BorderColor = System.Drawing.Color.White;
            this.skinLabel2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel2.Location = new System.Drawing.Point(22, 100);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(68, 17);
            this.skinLabel2.TabIndex = 3;
            this.skinLabel2.Text = "出入口名称";
            // 
            // skinTextBoxGuardName
            // 
            this.skinTextBoxGuardName.BackColor = System.Drawing.Color.Transparent;
            this.skinTextBoxGuardName.DownBack = null;
            this.skinTextBoxGuardName.Icon = null;
            this.skinTextBoxGuardName.IconIsButton = false;
            this.skinTextBoxGuardName.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.skinTextBoxGuardName.IsPasswordChat = '\0';
            this.skinTextBoxGuardName.IsSystemPasswordChar = false;
            this.skinTextBoxGuardName.Lines = new string[0];
            this.skinTextBoxGuardName.Location = new System.Drawing.Point(109, 94);
            this.skinTextBoxGuardName.Margin = new System.Windows.Forms.Padding(0);
            this.skinTextBoxGuardName.MaxLength = 32767;
            this.skinTextBoxGuardName.MinimumSize = new System.Drawing.Size(28, 28);
            this.skinTextBoxGuardName.MouseBack = null;
            this.skinTextBoxGuardName.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.skinTextBoxGuardName.Multiline = false;
            this.skinTextBoxGuardName.Name = "skinTextBoxGuardName";
            this.skinTextBoxGuardName.NormlBack = null;
            this.skinTextBoxGuardName.Padding = new System.Windows.Forms.Padding(5);
            this.skinTextBoxGuardName.ReadOnly = false;
            this.skinTextBoxGuardName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.skinTextBoxGuardName.Size = new System.Drawing.Size(185, 28);
            // 
            // 
            // 
            this.skinTextBoxGuardName.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.skinTextBoxGuardName.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTextBoxGuardName.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.skinTextBoxGuardName.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.skinTextBoxGuardName.SkinTxt.Name = "BaseText";
            this.skinTextBoxGuardName.SkinTxt.Size = new System.Drawing.Size(175, 18);
            this.skinTextBoxGuardName.SkinTxt.TabIndex = 0;
            this.skinTextBoxGuardName.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.skinTextBoxGuardName.SkinTxt.WaterText = "";
            this.skinTextBoxGuardName.TabIndex = 4;
            this.skinTextBoxGuardName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.skinTextBoxGuardName.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.skinTextBoxGuardName.WaterText = "";
            this.skinTextBoxGuardName.WordWrap = true;
            // 
            // skinTextBoxIpAddress
            // 
            this.skinTextBoxIpAddress.BackColor = System.Drawing.Color.Transparent;
            this.skinTextBoxIpAddress.DownBack = null;
            this.skinTextBoxIpAddress.Icon = null;
            this.skinTextBoxIpAddress.IconIsButton = false;
            this.skinTextBoxIpAddress.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.skinTextBoxIpAddress.IsPasswordChat = '\0';
            this.skinTextBoxIpAddress.IsSystemPasswordChar = false;
            this.skinTextBoxIpAddress.Lines = new string[0];
            this.skinTextBoxIpAddress.Location = new System.Drawing.Point(109, 139);
            this.skinTextBoxIpAddress.Margin = new System.Windows.Forms.Padding(0);
            this.skinTextBoxIpAddress.MaxLength = 32767;
            this.skinTextBoxIpAddress.MinimumSize = new System.Drawing.Size(28, 28);
            this.skinTextBoxIpAddress.MouseBack = null;
            this.skinTextBoxIpAddress.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.skinTextBoxIpAddress.Multiline = false;
            this.skinTextBoxIpAddress.Name = "skinTextBoxIpAddress";
            this.skinTextBoxIpAddress.NormlBack = null;
            this.skinTextBoxIpAddress.Padding = new System.Windows.Forms.Padding(5);
            this.skinTextBoxIpAddress.ReadOnly = false;
            this.skinTextBoxIpAddress.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.skinTextBoxIpAddress.Size = new System.Drawing.Size(185, 28);
            // 
            // 
            // 
            this.skinTextBoxIpAddress.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.skinTextBoxIpAddress.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTextBoxIpAddress.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.skinTextBoxIpAddress.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.skinTextBoxIpAddress.SkinTxt.Name = "BaseText";
            this.skinTextBoxIpAddress.SkinTxt.Size = new System.Drawing.Size(175, 18);
            this.skinTextBoxIpAddress.SkinTxt.TabIndex = 0;
            this.skinTextBoxIpAddress.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.skinTextBoxIpAddress.SkinTxt.WaterText = "";
            this.skinTextBoxIpAddress.TabIndex = 6;
            this.skinTextBoxIpAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.skinTextBoxIpAddress.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.skinTextBoxIpAddress.WaterText = "";
            this.skinTextBoxIpAddress.WordWrap = true;
            // 
            // skinLabel3
            // 
            this.skinLabel3.AutoSize = true;
            this.skinLabel3.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel3.BorderColor = System.Drawing.Color.White;
            this.skinLabel3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel3.Location = new System.Drawing.Point(37, 145);
            this.skinLabel3.Name = "skinLabel3";
            this.skinLabel3.Size = new System.Drawing.Size(43, 17);
            this.skinLabel3.TabIndex = 5;
            this.skinLabel3.Text = "IP地址";
            // 
            // skinLabel4
            // 
            this.skinLabel4.AutoSize = true;
            this.skinLabel4.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel4.BorderColor = System.Drawing.Color.White;
            this.skinLabel4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel4.Location = new System.Drawing.Point(22, 191);
            this.skinLabel4.Name = "skinLabel4";
            this.skinLabel4.Size = new System.Drawing.Size(56, 17);
            this.skinLabel4.TabIndex = 9;
            this.skinLabel4.Text = "相机类型";
            // 
            // skinRadioButtonOtherCamera
            // 
            this.skinRadioButtonOtherCamera.AutoSize = true;
            this.skinRadioButtonOtherCamera.BackColor = System.Drawing.Color.Transparent;
            this.skinRadioButtonOtherCamera.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinRadioButtonOtherCamera.DownBack = null;
            this.skinRadioButtonOtherCamera.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinRadioButtonOtherCamera.Location = new System.Drawing.Point(178, 189);
            this.skinRadioButtonOtherCamera.MouseBack = null;
            this.skinRadioButtonOtherCamera.Name = "skinRadioButtonOtherCamera";
            this.skinRadioButtonOtherCamera.NormlBack = null;
            this.skinRadioButtonOtherCamera.SelectedDownBack = null;
            this.skinRadioButtonOtherCamera.SelectedMouseBack = null;
            this.skinRadioButtonOtherCamera.SelectedNormlBack = null;
            this.skinRadioButtonOtherCamera.Size = new System.Drawing.Size(62, 21);
            this.skinRadioButtonOtherCamera.TabIndex = 8;
            this.skinRadioButtonOtherCamera.TabStop = true;
            this.skinRadioButtonOtherCamera.Text = "副相机";
            this.skinRadioButtonOtherCamera.UseVisualStyleBackColor = false;
            // 
            // skinRadioButtonMainCamera
            // 
            this.skinRadioButtonMainCamera.AutoSize = true;
            this.skinRadioButtonMainCamera.BackColor = System.Drawing.Color.Transparent;
            this.skinRadioButtonMainCamera.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinRadioButtonMainCamera.DownBack = null;
            this.skinRadioButtonMainCamera.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinRadioButtonMainCamera.Location = new System.Drawing.Point(109, 189);
            this.skinRadioButtonMainCamera.MouseBack = null;
            this.skinRadioButtonMainCamera.Name = "skinRadioButtonMainCamera";
            this.skinRadioButtonMainCamera.NormlBack = null;
            this.skinRadioButtonMainCamera.SelectedDownBack = null;
            this.skinRadioButtonMainCamera.SelectedMouseBack = null;
            this.skinRadioButtonMainCamera.SelectedNormlBack = null;
            this.skinRadioButtonMainCamera.Size = new System.Drawing.Size(62, 21);
            this.skinRadioButtonMainCamera.TabIndex = 7;
            this.skinRadioButtonMainCamera.TabStop = true;
            this.skinRadioButtonMainCamera.Text = "主相机";
            this.skinRadioButtonMainCamera.UseVisualStyleBackColor = false;
            // 
            // skinTextBoxScreenIP
            // 
            this.skinTextBoxScreenIP.BackColor = System.Drawing.Color.Transparent;
            this.skinTextBoxScreenIP.DownBack = null;
            this.skinTextBoxScreenIP.Icon = null;
            this.skinTextBoxScreenIP.IconIsButton = false;
            this.skinTextBoxScreenIP.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.skinTextBoxScreenIP.IsPasswordChat = '\0';
            this.skinTextBoxScreenIP.IsSystemPasswordChar = false;
            this.skinTextBoxScreenIP.Lines = new string[0];
            this.skinTextBoxScreenIP.Location = new System.Drawing.Point(109, 224);
            this.skinTextBoxScreenIP.Margin = new System.Windows.Forms.Padding(0);
            this.skinTextBoxScreenIP.MaxLength = 32767;
            this.skinTextBoxScreenIP.MinimumSize = new System.Drawing.Size(28, 28);
            this.skinTextBoxScreenIP.MouseBack = null;
            this.skinTextBoxScreenIP.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.skinTextBoxScreenIP.Multiline = false;
            this.skinTextBoxScreenIP.Name = "skinTextBoxScreenIP";
            this.skinTextBoxScreenIP.NormlBack = null;
            this.skinTextBoxScreenIP.Padding = new System.Windows.Forms.Padding(5);
            this.skinTextBoxScreenIP.ReadOnly = false;
            this.skinTextBoxScreenIP.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.skinTextBoxScreenIP.Size = new System.Drawing.Size(185, 28);
            // 
            // 
            // 
            this.skinTextBoxScreenIP.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.skinTextBoxScreenIP.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTextBoxScreenIP.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.skinTextBoxScreenIP.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.skinTextBoxScreenIP.SkinTxt.Name = "BaseText";
            this.skinTextBoxScreenIP.SkinTxt.Size = new System.Drawing.Size(175, 18);
            this.skinTextBoxScreenIP.SkinTxt.TabIndex = 0;
            this.skinTextBoxScreenIP.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.skinTextBoxScreenIP.SkinTxt.WaterText = "";
            this.skinTextBoxScreenIP.TabIndex = 8;
            this.skinTextBoxScreenIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.skinTextBoxScreenIP.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.skinTextBoxScreenIP.WaterText = "";
            this.skinTextBoxScreenIP.WordWrap = true;
            // 
            // skinLabel5
            // 
            this.skinLabel5.AutoSize = true;
            this.skinLabel5.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel5.BorderColor = System.Drawing.Color.White;
            this.skinLabel5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel5.Location = new System.Drawing.Point(37, 230);
            this.skinLabel5.Name = "skinLabel5";
            this.skinLabel5.Size = new System.Drawing.Size(43, 17);
            this.skinLabel5.TabIndex = 7;
            this.skinLabel5.Text = "屏幕IP";
            // 
            // skinButtonSave
            // 
            this.skinButtonSave.BackColor = System.Drawing.Color.Transparent;
            this.skinButtonSave.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(221)))), ((int)(((byte)(223)))));
            this.skinButtonSave.BorderColor = System.Drawing.Color.Gray;
            this.skinButtonSave.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButtonSave.DownBack = null;
            this.skinButtonSave.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.skinButtonSave.FadeGlow = false;
            this.skinButtonSave.ForeColor = System.Drawing.Color.Black;
            this.skinButtonSave.GlowColor = System.Drawing.Color.Black;
            this.skinButtonSave.IsDrawGlass = false;
            this.skinButtonSave.Location = new System.Drawing.Point(84, 291);
            this.skinButtonSave.MouseBack = null;
            this.skinButtonSave.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.skinButtonSave.Name = "skinButtonSave";
            this.skinButtonSave.NormlBack = null;
            this.skinButtonSave.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinButtonSave.Size = new System.Drawing.Size(75, 23);
            this.skinButtonSave.TabIndex = 10;
            this.skinButtonSave.Text = "保存";
            this.skinButtonSave.UseVisualStyleBackColor = false;
            this.skinButtonSave.Click += new System.EventHandler(this.skinButtonSave_Click);
            // 
            // skinButtonCancel
            // 
            this.skinButtonCancel.BackColor = System.Drawing.Color.Transparent;
            this.skinButtonCancel.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(221)))), ((int)(((byte)(223)))));
            this.skinButtonCancel.BorderColor = System.Drawing.Color.Gray;
            this.skinButtonCancel.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButtonCancel.DownBack = null;
            this.skinButtonCancel.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.skinButtonCancel.FadeGlow = false;
            this.skinButtonCancel.ForeColor = System.Drawing.Color.Black;
            this.skinButtonCancel.GlowColor = System.Drawing.Color.Black;
            this.skinButtonCancel.IsDrawGlass = false;
            this.skinButtonCancel.Location = new System.Drawing.Point(198, 291);
            this.skinButtonCancel.MouseBack = null;
            this.skinButtonCancel.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.skinButtonCancel.Name = "skinButtonCancel";
            this.skinButtonCancel.NormlBack = null;
            this.skinButtonCancel.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.skinButtonCancel.TabIndex = 11;
            this.skinButtonCancel.Text = "取消";
            this.skinButtonCancel.UseVisualStyleBackColor = false;
            // 
            // FrmGuard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 351);
            this.Controls.Add(this.skinButtonCancel);
            this.Controls.Add(this.skinButtonSave);
            this.Controls.Add(this.skinTextBoxScreenIP);
            this.Controls.Add(this.skinLabel5);
            this.Controls.Add(this.skinLabel4);
            this.Controls.Add(this.skinRadioButtonOtherCamera);
            this.Controls.Add(this.skinRadioButtonMainCamera);
            this.Controls.Add(this.skinTextBoxIpAddress);
            this.Controls.Add(this.skinLabel3);
            this.Controls.Add(this.skinTextBoxGuardName);
            this.Controls.Add(this.skinLabel2);
            this.Controls.Add(this.skinLabel1);
            this.Controls.Add(this.skinRadioButton2);
            this.Controls.Add(this.skinRadioButton1);
            this.Name = "FrmGuard";
            this.ShowDrawIcon = false;
            this.ShowIcon = false;
            this.Text = "相机设置";
            this.Load += new System.EventHandler(this.FrmGuard_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CCWin.SkinControl.SkinRadioButton skinRadioButton1;
        private CCWin.SkinControl.SkinRadioButton skinRadioButton2;
        private CCWin.SkinControl.SkinLabel skinLabel1;
        private CCWin.SkinControl.SkinLabel skinLabel2;
        private CCWin.SkinControl.SkinTextBox skinTextBoxGuardName;
        private CCWin.SkinControl.SkinTextBox skinTextBoxIpAddress;
        private CCWin.SkinControl.SkinLabel skinLabel3;
        private CCWin.SkinControl.SkinLabel skinLabel4;
        private CCWin.SkinControl.SkinRadioButton skinRadioButtonOtherCamera;
        private CCWin.SkinControl.SkinRadioButton skinRadioButtonMainCamera;
        private CCWin.SkinControl.SkinTextBox skinTextBoxScreenIP;
        private CCWin.SkinControl.SkinLabel skinLabel5;
        private CCWin.SkinControl.SkinButton skinButtonSave;
        private CCWin.SkinControl.SkinButton skinButtonCancel;
    }
}