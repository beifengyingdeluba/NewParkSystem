﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NewParkSystem.Models;
using CCWin;

namespace NewParkSystem.Forms
{
    public partial class FrmGuard : Skin_Mac
    {

        public FrmGuard()
        {
            InitializeComponent();
        }

        private void FrmGuard_Load(object sender, EventArgs e)
        {

        }

        private void skinButtonSave_Click(object sender, EventArgs e)
        {
            //192.168.88.213
            try
            {
                Guard guard = null;
                if (Params.Settings.Guards != null)
                {
                    guard = Params.Settings.Guards.FirstOrDefault(a => a.Primary.IP == skinTextBoxIpAddress.Text.Trim());
                }
                else
                {
                    Params.Settings.Guards = new List<Guard>();
                }

                GuardItem guardItem = new GuardItem();
                if (guard == null)
                {
                    guard = new Guard();
                    guard.IsExit = skinRadioButton1.Checked;
                    guard.No = skinTextBoxGuardName.Text.Trim();
                    guardItem.IP = skinTextBoxIpAddress.Text.Trim();
                    guardItem.ScreenIP = skinTextBoxScreenIP.Text.Trim();
                    guard.Primary = guardItem;
                    Params.Settings.Guards.Add(guard);
                    MessageBox.Show("保存成功");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("此IP已存在");
                }
            }
            catch (Exception ex)
            {
            }

        }
    }
}
