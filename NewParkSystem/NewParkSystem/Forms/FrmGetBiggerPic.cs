﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CCWin;

namespace NewParkSystem.Forms
{
    public partial class FrmGetBiggerPic : Skin_Mac
    {
        private string _picUrl = string.Empty;
        public FrmGetBiggerPic(string picUrl)
        {
            InitializeComponent();
            _picUrl = picUrl;
        }

        private void FrmGetBiggerPic_Load(object sender, EventArgs e)
        {
            skinPictureBox1.Load(_picUrl);
        }
    }
}
